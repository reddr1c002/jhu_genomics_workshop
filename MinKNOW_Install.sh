g="$(which python)"
baseBin="$(dirname $g)"
src_bin="${baseBin}/../src_custom"
mkdir $src_bin
script_location="$(perl -MCwd=abs_path -le 'print abs_path(shift)' $(which $(basename $0)))"


###Install minknow software from https://github.com/rrwick/MinION-desktop/  github repo
#this will currently only work for linux users
if [[ ! -e "/opt/ont/minknow-ui/MinKNOW" ]]; then	
	#install MinKNOW
	wget -O- https://mirror.oxfordnanoportal.com/apt/ont-repo.pub | sudo apt-key add -
	echo "deb http://mirror.oxfordnanoportal.com/apt xenial-stable non-free" | sudo tee /etc/apt/sources.list.d/nanoporetech.sources.list
	sudo apt-get update
	sudo apt-get -y install minknow-nc
	sudo apt-get -y install minknow-nc

fi
# if ! [  -d ${Database_location}minikraken_2017* ]; then
# 	wget https://ccb.jhu.edu/software/kraken/dl/minikraken_20171019_4GB.tgz -P $Database_location
# 	tar -xvzf ${Database_location}minikraken_20171019_4GB.tgz  --directory $Database_location
# 	rm ${Database_location}minikraken_20171019_4GB.tgz 
# fi


#Enable offline mode for MinKNOW Software
#Deprecated?
bash linux_offline_minknow.sh
# source $HOME/.bashrc
