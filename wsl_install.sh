
read -r -p "is the install designed for WSL (Windows Subystem for Linux? If \"y\", 
the system will finish installing after setting up
the proper WSL environment. 
Select \"n\" if you arent using WSL OR you've already selected/previously \"y\" to 
set up the WSL environment [y/n]" response
if [[ "$response" =~ ^([yY][eE][sS]|[yY])+$ ]];
then
	if ! grep -L  "echo export DISPLAY=:0.0" ~/.bashrc; then
		echo export DISPLAY=:0.0 >> ~/.bashrc
		source ~/.bashrc
	fi
	conda activate jhu_workshop
	cd app
	npm uninstall electron
	npm install electron@5.0.4

	#This is a very large, bundled package. It might not be necessary to intall ALL of the .deb packages in the source from this
	# sudo chown root app/node_modules/electron/dist/chrome-sandbox

	cd ..
	sudo chmod 4755 app/node_modules/electron/dist/chrome-sandbox
	sed -i "s/).then((dir)=>{/,function(dir){ dir ={"filePaths": dir[0]};/g" app/renderer.js
	
	echo -e "\n\nDone Installing, dont forget to check WSL document in base directory for further instructions.\nNow you can rerun the Base_install.sh command but this time select [N] at the initial prompt\n"
	exit 1
fi