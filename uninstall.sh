#!/bin/bash


echo "removing post-hoc dependencies"
sudo rm -rf /opt/ONT/MinKNOW
sudo apt-get -y purge ont-bream4-minion
sudo apt-get -y purge minknow-nc
sudo apt-get -y purge minknow-core-minion-nc
sudo apt-get -y purge minion-nc
sudo apt-get -y purge ont-minknow-frontend
sudo apt-get -y purge ont-guppy
sudo rm -rf /opt/ONT/MinKNOW
#remove minknow makes an authentication error/corruption of gdm3, so purge and reinstall
sudo rm -rf /usr/bin/aliview 


