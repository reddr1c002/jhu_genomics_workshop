#!/usr/bin/expect -f
set MYVAR [lindex $argv 0]
set MYVAR2 [lindex $argv 1]


# Get a Bash shell
spawn -noecho bash

# Wait for a prompt
expect "$ "

# Type something
send -- "conda activate jhu_workshop && blastn \\\n  -db nt  \\\n -query $MYVAR \\\n  -remote \\\n  -out $MYVAR2"
# Hand over control to the user
interact

exit