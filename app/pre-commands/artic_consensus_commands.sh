#!/usr/bin/expect -f
set MYVAR [lindex $argv 0]
set MYVAR2 [lindex $argv 1]
set MYVAR3 [lindex $argv 2]

# Get a Bash shell
spawn -noecho bash

# Wait for a prompt
expect "$ "

# Type something
send -- "conda deactivate && conda activate artic-rampart-apl && $MYVAR \\\n\t $MYVAR2 $MYVAR3 && echo done > done"
# Hand over control to the user
interact

exit
