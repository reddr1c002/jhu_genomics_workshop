#!/usr/bin/expect -f
set MYVAR [lindex $argv 0]
set MYVAR1 [lindex $argv 1]
set MYVAR2 [lindex $argv 2]


# Get a Bash shell
spawn -noecho bash

# Wait for a prompt
expect "$ "

# Type something
send -- "conda activate jhu_workshop && minimap2 \\\n\t -x map-ont \\\n\t -a \\\n\t -o ${MYVAR2}.sam \\\n\t --cs \\\n\t \"$MYVAR\"  \\\n\t \"$MYVAR1\" 2> ${MYVAR2}.minimap2.log"
# Hand over control to the user
interact

exit
