#!/usr/bin/expect -f
set MYVAR [lindex $argv 0]
set MYVAR2 [lindex $argv 1]
set MYVAR3 [lindex $argv 2]
set MYVAR4 [lindex $argv 3]
set MYVAR5 [lindex $argv 4]

# Get a Bash shell
spawn -noecho bash

# Wait for a prompt
expect "$ "

# Type something
send -- "conda activate jhu_workshop && $MYVAR \\\n\t $MYVAR2 $MYVAR3 \\\n\t $MYVAR4 $MYVAR5 && echo done > done"
# Hand over control to the user
interact

exit
