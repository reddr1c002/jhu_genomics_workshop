#!/usr/bin/expect -f
set MYVAR [lindex $argv 0]
set MYVAR2 [lindex $argv 1]


# Get a Bash shell
spawn -noecho bash

# Wait for a prompt
expect "$ "

# Type something
send -- "bash process_krakendb.sh \\\n  -k $MYVAR"
# Hand over control to the user
interact

exit