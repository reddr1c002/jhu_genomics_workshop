#!/usr/bin/expect -f
set MYVAR [lindex $argv 0]
set MYVAR1 [lindex $argv 1]
set MYVAR2 [lindex $argv 2]
set MYVAR3 [lindex $argv 3]
set SCRIPTVAR1 [lindex $argv 4]

# Get a Bash shell
spawn -noecho bash

# Wait for a prompt
expect "$ "

# Type something
send -- "conda activate jhu_workshop && bash $SCRIPTVAR1 \\\n -f $MYVAR \\\n -r $MYVAR1  \\\n -k $MYVAR2 \\\n -o $MYVAR3"
# Hand over control to the user
interact

exit
