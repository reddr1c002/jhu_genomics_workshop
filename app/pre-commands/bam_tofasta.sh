#!/usr/bin/expect -f
set MYVAR [lindex $argv 0]
set MYVAR1 [lindex $argv 1]
set MYVAR2 [lindex $argv 2]
set MYVAR3 [lindex $argv 3]

set SCRIPTVAR1 [lindex $argv 4]
set SCRIPTVAR2 [lindex $argv 5]
# Get a Bash shell
spawn -noecho bash

# Wait for a prompt
expect "$ "

# Type something
send -- "conda activate jhu_workshop  && \\\n\t bash $SCRIPTVAR1 $MYVAR && \\\n\t bash $SCRIPTVAR2 \\\n\t -r $MYVAR1 \\\n\t -b $MYVAR2 -o $MYVAR3"
# Hand over control to the user
interact

exit
