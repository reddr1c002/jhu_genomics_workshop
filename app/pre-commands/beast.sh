#!/usr/bin/expect -f
set MYVAR [lindex $argv 0]


# Get a Bash shell
spawn -noecho bash

# Wait for a prompt
expect "$ "

# Type something
send  "exec beast"
# Hand over control to the user
interact

exit
