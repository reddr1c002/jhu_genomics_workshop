#!/bin/bash

#---------------------------------------------------------------------------------------------------
# deal with local environment

# pull nanosecond runtime for temporary file directory
runtime=$(date +"%Y%m%d-%H%M%S-%N")

# define colors for error messages
red='\033[0;31m'
RED='\033[1;31m'
green='\033[0;32m'
GREEN='\033[1;32m'
yellow='\033[0;33m'
YELLOW='\033[1;33m'
blue='\033[0;34m'
BLUE='\033[1;34m'
purple='\033[0;35m'
PURPLE='\033[1;35m'
cyan='\033[0;36m'
CYAN='\033[1;36m'
NC='\033[0m'

# usage function
usage() {
	echo -e "usage: ${YELLOW}$0${NC} [options]"
	echo -e ""
	echo -e "OPTIONS:"
	echo -e "   -h      show this message"
	echo -e "   -f      FASTQ file"
	echo -e "   -k      kraken output file"
	echo -e "   -r      kraken report with fullstring"
	echo -e "   -o      output directory (default: ${CYAN}./${NC})"
	echo -e "   -l      logfile"
	echo -e "   -w      temporary directory (default: ${CYAN}/tmp${NC})"
	echo -e ""
}

#---------------------------------------------------------------------------------------------------
# set default values here
tempdir="/tmp"
outputPath="./"

# parse input arguments
while getopts "hf:k:r:o:l:w:" OPTION
do
	case $OPTION in
		h) usage; exit 1 ;;
		f) fastq=$OPTARG ;;
		k) kraken=$OPTARG ;;
		r) fullstring=$OPTARG ;;
		o) outputPath=$OPTARG ;;
		l) logfile=$OPTARG ;;
		w) tempdir=$OPTARG ;;
		?) usage; exit 1 ;;
	esac
done

# make sure FASTQ file exists
if ! [[ -f "$fastq" ]]; then
	echo -e "\n${RED}Error: specified FASTQ file ${YELLOW}\"$fastq\"${NC} does not exist.${NC}\n" >&2
	usage
	exit 2
fi

# make sure kraken file exists
if ! [[ -f "$kraken" ]]; then
	echo -e "\n${RED}Error: specified kraken file ${YELLOW}\"$kraken\"${NC} does not exist.${NC}\n" >&2
	usage
	exit 2
fi

# make sure kraken report file exists
if ! [[ -f "$fastq" ]]; then
	echo -e "\n${RED}Error: specified fullstring kraken report ${YELLOW}\"$fullstring\"${NC} does not exist.${NC}\n" >&2
	usage
	exit 2
fi

# make sure output directory does not exist
if [[ -d "$outputPath" ]]; then
	echo -e "\n${YELLOW}Warning: specified output directory already exists.${NC}\n" >&2
fi

# define default logfile
if [[ -z "$logfile" ]]; then
	logfile="$outputPath/split_segments.log"
fi

#---------------------------------------------------------------------------------------------------
# error checking


#---------------------------------------------------------------------------------------------------
# dependency checking (not implemented yet)


#===================================================================================================
# defining functions
#===================================================================================================

echo_log() {

	input="$*"

	# if input is non-empty string, prepend initial space
	if [[ -n "$input" ]]; then
		input=" $input"
	fi

	# print to STDOUT
	echo -e "[$(date +"%F %T")]$input"

	# print to log file (after removing color strings)
	echo -e "[$(date +"%F %T")]$prefix$input" | gawk '{ printf("%s\n", gensub(/\x1b\[[0-9;]*m?/, "", "g", $0)); }' >> "$logfile"
}

#---------------------------------------------------------------------------------------------------
# convert FASTQ to four column file
fastq_4col() {

	input="$*"

	gawk '{
		if(NR == 1) {
			printf("%s", $0);
		} else {
			if(substr($0,1,1) == "@") {
				printf("\n%s", $0);
			} else {
				printf("\t%s", $0);
			}
		}
	} END {
		printf("\n");
	}' "$input"

}

#---------------------------------------------------------------------------------------------------

#===================================================================================================
# Main body
#===================================================================================================

# create environment
workdir="$tempdir/split_segments-$runtime"
mkdir -p "$workdir"
mkdir -p "$outputPath"

#===================================================================================================

# report current hash of git repo
#GIT_DIR="$(dirname $(readlink -f $(which $(basename $0))))/.git"
#export GIT_DIR
#hash=$(git rev-parse --short HEAD)
hash="NA"

# set up log file
echo_log "------ Call to "${YELLOW}$(basename $0)${NC}" from "${GREEN}$(hostname)${NC}" ------"
echo_log "current git hash: $hash"
echo_log "input arguments:"
echo_log "  FASTQ file: ${CYAN}$fastq${NC}"
echo_log "  kraken output: ${CYAN}$kraken${NC}"
echo_log "  kraken report: ${CYAN}$fullstring${NC}"
echo_log "output files:"
echo_log "  output directory: ${CYAN}$outputPath${NC}"
echo_log "  log file: ${CYAN}$logfile${NC}"
echo_log "------ beginning analysis ------"

# set up intermediate file
joined="$workdir/$(basename $fastq).kraken_join"

# add kraken classification to FASTQ file
gawk -F $'\t' '{
	if(NR==FNR) {
		classification[$2] = $3;
	} else {
		split(substr($1,2), h, " ");
		header=h[1];
		printf("%s\t%s\t%s\t%s\n", classification[header], $1, $2, $4);
	}
}' "$kraken" <(fastq_4col "$fastq") > "$joined"

# split FASTQ file by reads classified to a flu segment
gawk -F $'\t' -v OUTPATH="$outputPath" -v BASE=$(basename "${fastq%.*}") 'BEGIN {
	file=0;
} {
	if(FNR==1) {
		file += 1;
	}
	switch(file) {
		case 1:
			fullstring[$5] = $7;
			if($7 ~ /\(segment\)$/) {
				segment[$5] = gensub(/^ +([1-8]) \((.*)\)/, "\\1_\\2", "g", $6);
				flu_type[$5] = gensub(/.*Influenza ([A-D]) virus\(species\)\|.*/, "\\1", "g", $7);
			}
			break;
		case 2:
			if(fullstring[$1] ~ /\(segment\)/) {
				segment_taxid = gensub(/.*\|([0-9]+);[^\|]+\(segment\).*/, "\\1", "g", fullstring[$1]);
				if(length(flu_type[segment_taxid]) == 1) {
					filename=sprintf("%s/%s-%s-%s.fastq", OUTPATH, BASE, flu_type[segment_taxid], segment[segment_taxid]);
					printf("%s\n%s\n+\n%s\n", $2, $3, $4) >> filename;
				}
			}
			break;
	}
}' "$fullstring" "$joined"

rm -rf "$workdir"

echo_log "${GREEN}Done "$(basename $0)".${NC}"

#--eof--#
