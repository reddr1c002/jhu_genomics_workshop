\#The JHU Genomics Workshop app is designed primarily in the Ubuntu
(Linux) environment. As such, it uses many packages and dependencies
that aren’t available in a non-Unix environment. However, Windows
recently released a (beta) subsystem which allows users to, natively,
run linux in their own Windows Environment. If one does not want to
download and run a Virtual Machine with Ubuntu guest OS, this is an
alternative option/solution for that.

\#Because Windows Subsystem for Linux is still in beta, it can be fairly
buggy. However, we have designed this tutorial to be as seamless and
automated for the end-user as possible but cannot guarantee that
situations may arise that vary across Windows-hosted devices due to
limitations such as network or hardware.

\#Setup/Installation

\#First, enable WSL at Control panel -&gt; Programs -&gt; Turn Windows
Feature on or off -&gt; At bottom (check it)

![](media/image1.png){width="4.5in" height="0.96875in"}

![](media/image2.png){width="6.5in" height="0.9013888888888889in"}

![](media/image3.png){width="5.75in" height="1.6125in"}

\#Enable Developer mode at Settings -&gt; For Developers (top section).
This may require a restart of your operating system/computer.

![](media/image4.png){width="6.5in" height="3.2243055555555555in"}

\#Next, download Ubuntu 18.04 from windows store

![](media/image5.png){width="3.375in" height="2.2583333333333333in"}

\#install the system as designated and then run bash.exe (or type ubuntu
in search bar to open)

![](media/image6.png){width="5.104166666666667in"
height="2.078472222222222in"}

\#Allow it to install and set Username and Password accordingly. You can
leave many of the optional information points blank for the time-being.

!\# If you are not able to access Windows Store or it is failing you can
install via cmd in windows by opening cmd (type cmd in search bar on
Windows) and type: \`lxrun /install\`

![](media/image7.png){width="6.5in"
height="2.0194444444444444in"}\#Using this method, you will have to
input your user information on Linux startup

\#Now, we will open up our subsystem by typing bash.exe in the search
bar (as described above). From there, you should see a black
terminal/window appear. Don’t worry if the green letters don’t coincide
with your own personal information.

![](media/image8.png){width="6.5in" height="1.2770833333333333in"}\#We
will come back to this for now but keep this window open for now.

\# To run Graphical User Interfaces (GUI’s), you need to use an xServer.
You have the option of downloading 2 types of software to accomplish
this. The first, XServer, provides a lightweight, unobtrusive but less
customizable or interactive experience with its own GUI.

\#The less interactive one will require you to download and Install
XServer from : https://sourceforge.net/projects/vcxsrv/ on your Windows
Machine. Then unzip and run the software by typing \`xlaunch\` in your
Windows search-bar.

![](media/image9.png){width="6.5in" height="1.5805555555555555in"}

![](media/image10.png){width="3.4472222222222224in"
height="2.6486111111111112in"}

![](media/image12.png){width="3.40625in" height="2.6618055555555555in"}

![](media/image13.png){width="3.5395833333333333in"
height="2.7333333333333334in"}

And finally hit launch! GUI’s will not auto-appear in Windows when the
correct command is given in the subsystem.

\#If you want a more interactive environment opt for mobaXterm. This
method is designed for users with less experience with the command line.

\#First, download the FREE mobaxterm software at:
https://mobaxterm.mobatek.net/download.html

![](media/image14.png){width="6.113194444444445in"
height="3.0319444444444446in"}\#Then unzip the documents to a specified
folder of your choice. Launch the software.

![](media/image15.png){width="2.651388888888889in"
height="4.281944444444444in"}

\#Select the User Session on the left-hand side for Ubuntu and double
left-click

![](media/image16.png){width="6.5in" height="2.4131944444444446in"}

\#Once you have Ubuntu 18.04 and your xServer enabled with one of the 2
softwares, it is now time to enter the subsystem on Windows and begin
the app’s installation process!

\#Go back to your Sybsystem window. This will be on your toolbar if
you’re using either software but if you’re using mobaXTerm we recommend
using the terminal that pop-ups in that software to do all of your
commands.

\#If you already have WSL setup for previous work, skip these next few
steps in creating the directories and update/upgrade. Start with the git
command(s). First lets clone the repository that contains our app after
setting up a directory/environment to place it in. Hit:

\`cd \~\`

\`mkdir Documents\`

\`cd Documents\`

\`sudo apt-get -y install update\`

\`sudo apt-get -y upgrade\`

\#Now, we simply need to install the command: \`git\` to clone our
repository. This should be installed on your system by default, but
let’s be sure it is by typing:

\`sudo apt-get install git\`

\#Now, clone the repository with git

\`git clone
https://bmerr@bitbucket.org/bmerr/jhu\_genomics\_workshop.git\`

\`cd jhu\_genomics\_workshop/Install\_Scripts\`

\#Next, type \`bash Base\_install.sh\` and enter y (yes) command at the
prompt to set up the correct environment and packages to run the
genomics software/pipeline before running the base installation process
of the app. Be sure to input your password when prompted at the
beginning. This is a lengthy process so monitor progress while sipping
your coffee or tea.

\#That’s it for the subsystem! Now, follow the standard instructions in
the README to continue the process i.e. hit N for the initial prompt
after rerunning Base\_install.sh! To Run the app, you will need to type:

\`electron . --no-sandbox\` inside of the jhu\_genomics\_workshop/app
folder rather than the default method from the README
