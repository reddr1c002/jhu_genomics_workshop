#!/bin/bash

# This script is our pipeline for processing an Oxford MinION run, either during or after the run

# LICENSE AND DISCLAIMER
#
# Copyright (c) 2019 The Johns Hopkins University/Applied Physics Laboratory
#
# This software was developed at The Johns Hopkins University/Applied Physics Laboratory ("JHU/APL") that is the author thereof under the "work made for hire" provisions of the copyright law. Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation (the "Software"), to use the Software without restriction, including without limitation the rights to copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit others to do so, subject to the following conditions:
#
# 1. This LICENSE AND DISCLAIMER, including the copyright notice, shall be included in all copies of the Software, including copies of substantial portions of the Software;
#
# 2. JHU/APL assumes no obligation to provide support of any kind with regard to the Software. This includes no obligation to provide assistance in using the Software nor to provide updated versions of the Software; and
#
# 3. THE SOFTWARE AND ITS DOCUMENTATION ARE PROVIDED AS IS AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES WHATSOEVER. ALL WARRANTIES INCLUDING, BUT NOT LIMITED TO, PERFORMANCE, MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND NONINFRINGEMENT ARE HEREBY DISCLAIMED. USERS ASSUME THE ENTIRE RISK AND LIABILITY OF USING THE SOFTWARE. USERS ARE ADVISED TO TEST THE SOFTWARE THOROUGHLY BEFORE RELYING ON IT. IN NO EVENT SHALL THE JOHNS HOPKINS UNIVERSITY BE LIABLE FOR ANY DAMAGES WHATSOEVER, INCLUDING, WITHOUT LIMITATION, ANY LOST PROFITS, LOST SAVINGS OR OTHER INCIDENTAL OR CONSEQUENTIAL DAMAGES, ARISING OUT OF THE USE OR INABILITY TO USE THE SOFTWARE.”

#---------------------------------------------------------------------------------------------------
# deal with local environment

# pull nanosecond runtime for temporary file directory
runtime=$(date +"%Y%m%d-%H%M%S-%N")

# identify location of Desktop
DESKTOP="$HOME/Desktop"

# define colors for error messages
red='\033[0;31m'
RED='\033[1;31m'
green='\033[0;32m'
GREEN='\033[1;32m'
yellow='\033[0;33m'
YELLOW='\033[1;33m'
blue='\033[0;34m'
BLUE='\033[1;34m'
purple='\033[0;35m'
PURPLE='\033[1;35m'
cyan='\033[0;36m'
CYAN='\033[1;36m'
NC='\033[0m'

# usage function
usage() {
	echo -e "usage: ${YELLOW}$0${NC} [options]"
	echo -e ""
	echo -e "OPTIONS:"
	echo -e "   -h      show this message"
	echo -e "   -p      run directory (i.e. a directory within ${CYAN}/mnt/c/MinION/reads${NC})"
	echo -e "   -k      kraken directory (default: ${CYAN}/data/ref/kraken/fludb_20190122${NC})"
	echo -e "   -w      temporary directory (default: ${CYAN}/tmp${NC})"
	echo -e "   -t      taxonomic ranks to pull out in report, comma-delimited (default: ${CYAN}superkingdom,phylum,class,order,family,genus,species${NC})"
	echo -e "   -s      summarize only after having processed all available FASTQ files"
	echo -e ""
}

#---------------------------------------------------------------------------------------------------
# set default values here
tempdir="/tmp"
ranks="superkingdom,phylum,class,order,family,genus,species"
prefix="BC" # I'm not sure if this can be set, but ultimately we want to tie this into a sample tracking file or database
summarize="true"
THREADS=$(if [[ $(nproc) -gt 1 ]]; then echo $(($(nproc)-1)); else echo 1; fi)
# parse input arguments
while getopts "hp:o:k:w:l:t:sd:" OPTION
do
	case $OPTION in
		h) usage; exit 1 ;;
		p) run_dir=$OPTARG ;;
		o) base_dir=$OPTARG ;;
		k) kraken_db=$OPTARG ;;
		w) tempdir=$OPTARG ;;
		l) logfile=$OPTARG ;;
		t) ranks=$OPTARG ;;
		s) summarize="false" ;;
		d) THREADS=$OPTARG ;;
		?) usage; exit 1 ;;
	esac
done

# make sure run directory exists
if ! [[ -d "$run_dir" ]]; then
	echo -e "\n${RED}Error: specified run directory \"$run_dir\" does not exist.${NC}\n" >&2
	usage
	exit 2
fi

# grab information about run
run_dir="${run_dir%/}"
base=$(find "$run_dir" -regextype posix-extended -regex ".*/fastq[_/]pass/.*.fastq" -printf "%f\n" | head -n1 | cut -d"_" -f1-3)

# set up working directory within temp directory
workdir="$tempdir/process_oxford-$runtime"
mkdir -p "$workdir"


# set up output directory for porechop directories and individual kraken files
kraken_db_root=$(basename "$kraken_db")
base_dir="$run_dir/output"
porechop_base="$base_dir/0-porechop"
fastq_base="$base_dir"
kraken_base="$base_dir/1-$kraken_db_root"
mkdir -p "$porechop_base"
mkdir -p "$fastq_base"
mkdir -p "$kraken_base"

# set up final output directory to copy JSONs for visualization
FINAL_OUTPUT="$DESKTOP/MinION_reports/$(basename $run_dir)/$kraken_db_root"
mkdir -p "$FINAL_OUTPUT/kraken_reports"

# define default logfile
if [[ -z "$logfile" ]]; then
	logfile="$base_dir/process_oxford.log"
fi

#---------------------------------------------------------------------------------------------------
# error checking

# make sure kraken database exists
if ! [[ -d "$kraken_db" ]]; then
	echo -e "\n${RED}Error: specified kraken database \"$kraken_db\" does not exist.${NC}\n" >&2
	usage
	exit 2
fi

# make sure kraken database has a taxonomy
if ! [[ -s "$kraken_db/taxonomy/names.dmp" && -s "$kraken_db/taxonomy/nodes.dmp" ]]; then
	echo -e "\n${RED}Error: specified kraken database \"$kraken_db\" does not have the required taxonomy files (names.dmp and nodes.dmp).${NC}\n" >&2
	exit 2
fi

# make sure kraken database has been built
if ! [[ -s "$kraken_db/database.kdb" && -s "$kraken_db/database.idx" ]]; then
	echo -e "\n${RED}Error: specified kraken database \"$kraken_db\" is not a valid kraken database.${NC}\n" >&2
	exit 2
fi

#---------------------------------------------------------------------------------------------------
# dependency checking (not implemented yet)

# porechop

# kraken, kraken-report

# krakenreport_fullstring.sh

# krakenreport2json.sh

#===================================================================================================
# defining functions
#===================================================================================================

echo_log() {

	input="$*"

	# if input is non-empty string, prepend initial space
	if [[ -n "$input" ]]; then
		input=" $input"
	fi

	# print to STDOUT
	echo -e "[$(date +"%F %T")]$input"

	# print to log file (after removing color strings)
	echo -e "[$(date +"%F %T")]$input\r" | sed -r 's/\x1b\[[0-9;]*m?//g' >> "$logfile"
}

summarize_all() {
	echo_log "    Summarizing output files..."
	find "$kraken_base" -maxdepth 1 -name "*.kraken" -print0 | \
		sort -n -z -T "$workdir" | while read -d $'\0' all_kraken; do
			echo_log "      $(basename $all_kraken)"
			outputbase="$all_kraken.report"
			kraken-report --db "$kraken_db" "$all_kraken" > "$outputbase"
			krakenreport_fullstring.sh -i "$outputbase" -k "$kraken_db" -o "$outputbase".full
			krakenreport2json.sh -i "$outputbase".full "$ranks" > "$outputbase".json
			cp "$outputbase" "$FINAL_OUTPUT/kraken_reports"
			cp "$outputbase".json "$FINAL_OUTPUT"
			echo_log "        done."
	done
}

#---------------------------------------------------------------------------------------------------
# demultiplex barcodes from single 4,000-read FASTQ file, classify with Kraken, and summarize to JSON
process_one() {

	fn="$1"
	gzip -d "$fn" 2> /dev/null
	fn="${fn%.gz}"
	base=$(basename "${fn%.fastq}")
	base="${base##*_}"
	echo_log "  ${YELLOW}Processing file $base...${NC}"

	# set up output directories
	fastq_dir="$porechop_base/porechop-75_5-$base"
	kraken_dir="$fastq_dir/$kraken_db_root"

	# demultiplex barcodes
	if ! [[ -f "$fastq_dir/porechop.done" ]]; then
		echo_log "    Demultiplexing with porechop"
		mkdir -p "$fastq_dir"
		porechop \
			--threads "$THREADS" \
			--input "$fn" \
			--check_reads 4000 \
			--barcode_dir "$fastq_dir" \
			--barcode_threshold 75 \
			--barcode_diff 5 \
			2>&1 | while read line; do echo -e "$line" | sed -r 's/\x1b\[[0-9;]*m?//g'; done > "$fastq_dir/porechop.log" && touch "$fastq_dir/porechop.done"
		echo_log "      done."

		# make sure reads were output
		if [[ -z $(find "$fastq_dir" -name "*.fastq") ]]; then
			cp "$fn" "$fastq_dir/none.fastq"
		fi
	else
		echo_log "    Porechop demultiplexing already complete."
	fi

	mkdir -p "$kraken_dir"

	# count the number of kraken output files that have already been added
	num_files=$(find "$fastq_dir" -regextype posix-extended -regex ".*.fastq(.gz)?" | wc -l)
	num_added=$(find "$kraken_dir" -name "*.kraken.added" | wc -l)

	# classify all demultiplexed FASTQ files
	if [[ "$num_added" -ge "$num_files" ]]; then
		echo_log "    FASTQ files already classified and included in summary ($num_added of $num_files)."
	else
		echo_log "    Classifying with Kraken against $kraken_db_root"
		find "$fastq_dir" -maxdepth 1 -name "*.fastq" -print0 | while read -d $'\0' fastq; do

			krakenbase=$(basename "${fastq%.fastq}.kraken")
			kraken="$kraken_dir/$krakenbase"

			echo_log "      $(basename $fastq)"
			if ! [[ -s "$kraken" ]]; then
				kraken --db "$kraken_db" --threads "$THREADS" --fastq-input "$fastq" 2>&1 > "$kraken" | while read line; do
					echo_log "        $line"
				done
			else
				echo_log "        already complete."
			fi

			if ! [[ -f "$fastq.added" ]]; then
				cat "$fastq" >> "$fastq_base"/$(basename "$fastq") && touch "$fastq.added" && gzip "$fastq"
			fi
			if ! [[ -f "$kraken.added" ]]; then
				cat "$kraken" >> "$kraken_base"/$(basename "$kraken") && touch "$kraken.added" && gzip "$kraken"
			fi
		done

		# summarize output files
		if [[ "$summarize" == "true" ]]; then
			summarize_all
		fi
	fi
}

#===================================================================================================
# Main body
#===================================================================================================

#===================================================================================================

# report current hash of git repo
#GIT_DIR="$(dirname $(readlink -f $(which $(basename $0))))/.git"
#export GIT_DIR
#hash=$(git rev-parse --short HEAD)
hash="NA"

# set up log file
echo_log "------ Call to "${YELLOW}$(basename $0)${NC}" from "${GREEN}$(hostname)${NC}" ------"
echo_log "current git hash: $hash"
echo_log "input arguments:"
echo_log "  run directory: ${CYAN}$run_dir${NC}"
echo_log "  kraken directory: ${CYAN}$kraken_db${NC}"
echo_log "output files:"
echo_log "  output directory: ${CYAN}$base_dir${NC}"
echo_log "  Windows output: ${CYAN}C:/${FINAL_OUTPUT#/mnt/c/}${NC}"
echo_log "  Linux output: ${CYAN}$run_dir${NC}"
echo_log "  log file: ${CYAN}$logfile${NC}"
echo_log "------ beginning analysis ------"

# process all FASTQ files
echo_log "Processing files"
find "$run_dir" -regextype posix-extended -regex ".*/fastq[_/]pass/.*.fastq" -print0 | \
	sort -n -z -T "$workdir" | while read -d $'\0' fastq; do
		process_one "$fastq"
done

if [[ "$summarize" == "false" ]]; then
	echo_log "  ${YELLOW}Summarizing samples...${NC}"
	summarize_all
fi
echo $workdir && rm -rf "$workdir"

echo_log "${GREEN}Done "$(basename $0)".${NC}"

#--eof--#
