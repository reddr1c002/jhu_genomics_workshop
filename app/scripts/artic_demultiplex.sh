#!/bin/bash

# This script is a wrapper for the 'artic demultiplex' script

# LICENSE AND DISCLAIMER
#
# Copyright (c) 2019 The Johns Hopkins University/Applied Physics Laboratory
#
# This software was developed at The Johns Hopkins University/Applied Physics Laboratory ("JHU/APL") that is the author thereof under the "work made for hire" provisions of the copyright law. Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation (the "Software"), to use the Software without restriction, including without limitation the rights to copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit others to do so, subject to the following conditions:
#
# 1. This LICENSE AND DISCLAIMER, including the copyright notice, shall be included in all copies of the Software, including copies of substantial portions of the Software;
#
# 2. JHU/APL assumes no obligation to provide support of any kind with regard to the Software. This includes no obligation to provide assistance in using the Software nor to provide updated versions of the Software; and
#
# 3. THE SOFTWARE AND ITS DOCUMENTATION ARE PROVIDED AS IS AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES WHATSOEVER. ALL WARRANTIES INCLUDING, BUT NOT LIMITED TO, PERFORMANCE, MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND NONINFRINGEMENT ARE HEREBY DISCLAIMED. USERS ASSUME THE ENTIRE RISK AND LIABILITY OF USING THE SOFTWARE. USERS ARE ADVISED TO TEST THE SOFTWARE THOROUGHLY BEFORE RELYING ON IT. IN NO EVENT SHALL THE JOHNS HOPKINS UNIVERSITY BE LIABLE FOR ANY DAMAGES WHATSOEVER, INCLUDING, WITHOUT LIMITATION, ANY LOST PROFITS, LOST SAVINGS OR OTHER INCIDENTAL OR CONSEQUENTIAL DAMAGES, ARISING OUT OF THE USE OR INABILITY TO USE THE SOFTWARE.”

#---------------------------------------------------------------------------------------------------
# deal with local environment

# pull nanosecond runtime for temporary file directory
runtime=$(date +"%Y%m%d-%H%M%S-%N")

# identify location of Desktop
DESKTOP="$HOME/Desktop"

# define colors for error messages
red='\033[0;31m'
RED='\033[1;31m'
green='\033[0;32m'
GREEN='\033[1;32m'
yellow='\033[0;33m'
YELLOW='\033[1;33m'
blue='\033[0;34m'
BLUE='\033[1;34m'
purple='\033[0;35m'
PURPLE='\033[1;35m'
cyan='\033[0;36m'
CYAN='\033[1;36m'
NC='\033[0m'

# usage function
usage() {
	echo -e "usage: ${YELLOW}$0${NC} [options]"
	echo -e ""
	echo -e "OPTIONS:"
	echo -e "   -h      show this message"
	echo -e "   -p      run directory (i.e. a directory within ${CYAN}/var/lib/MinKNOW/data${NC})"
	echo -e "   -o      output directory (default: ${CYAN}$DESKTOP/MinION_reports${NC})"
	echo -e "             Note: files will be placed in ${CYAN}<run-directory-base>/artic_demultiplex${NC} within the directory supplied with -o"
	echo -e "   -w      temporary directory (default: ${CYAN}/tmp${NC})"
	echo -e "   -d      threads (default: ${CYAN}$(($(nproc)-1))${NC} on this machine)"
	echo -e ""
}

#---------------------------------------------------------------------------------------------------
# set default values here
output_base="$DESKTOP/MinION_reports"
tempdir="/tmp"
THREADS=$(if [[ $(nproc) -gt 1 ]]; then echo $(($(nproc)-1)); else echo 1; fi)

# parse input arguments
while getopts "hp:o:l:w:d:" OPTION
do
	case $OPTION in
		h) usage; exit 1 ;;
		p) run_dir=$OPTARG ;;
		o) output_base=$OPTARG ;;
		l) logfile=$OPTARG ;;
		w) tempdir=$OPTARG ;;
		d) THREADS=$OPTARG ;;
		?) usage; exit 1 ;;
	esac
done

#---------------------------------------------------------------------------------------------------
# error checking

# make sure run directory exists
if ! [[ -d "$run_dir" ]]; then
	echo -e "\n${RED}Error: specified run directory \"$run_dir\" does not exist.${NC}\n" >&2
	usage
	exit 2
fi

# grab information about run
run_dir="${run_dir%/}"
fastq_dir=$(find "$run_dir" -regextype posix-extended -regex ".*/fastq[_/]pass/.*.fastq" -printf "%h\n" | head -n1)
run_base=$(basename "$run_dir")

base_dir="$output_base/$run_base"
gather_dir="$base_dir/artic_gather"

# make sure artic gather has been run
if ! [[ -d "$gather_dir" ]]; then
	echo -e "\n${RED}Error: specified run directory \"$base_dir\" does not contain the artic_gather directory.${NC}\n" >&2
	echo -e "\n${RED}       Please run ${YELLOW}artic_gather.sh${RED} before running this script.${NC}\n" >&2
	usage
	exit 2
fi

#---------------------------------------------------------------------------------------------------

# set up output directory for porechop directories and individual kraken files
demultiplex_dir="$base_dir/artic_gather"
mkdir -p "$demultiplex_dir"

# set up working directory within temp directory
workdir="$tempdir/artic_demultiplex-$runtime"
mkdir -p "$workdir"


# define default logfile
if [[ -z "$logfile" ]]; then
	logfile="$demultiplex_dir/artic_demultiplex.log"
fi

#---------------------------------------------------------------------------------------------------
# dependency checking

artic_check=$(artic demultiplex -h 2> /dev/null | wc -l)
if [[ "$artic_check" -eq 0 ]]; then
	echo -e "\n${RED}Error: ${YELLOW}artic demultiplex${RED} is not present in this environment.${NC}\n" >&2
	echo -e "\n${RED}       Make sure you have activated the right conda environment with ${YELLOW}conda activate env_name${RED}.${NC}\n" >&2
	exit 2
fi

#===================================================================================================
# defining functions
#===================================================================================================

echo_log() {

	input="$*"

	# if input is non-empty string, prepend initial space
	if [[ -n "$input" ]]; then
		input=" $input"
	fi

	# print to STDOUT
	echo -e "[$(date +"%F %T")]$input"

	# print to log file (after removing color strings)
	echo -e "[$(date +"%F %T")]$input\r" | sed -r 's/\x1b\[[0-9;]*m?//g' >> "$logfile"
}

#===================================================================================================
# Main body
#===================================================================================================

#===================================================================================================

# report current hash of git repo
#GIT_DIR="$(dirname $(readlink -f $(which $(basename $0))))/.git"
#export GIT_DIR
#hash=$(git rev-parse --short HEAD)

# set up log file
echo_log "------ Call to "${YELLOW}$(basename $0)${NC}" from "${GREEN}$(hostname)${NC}" ------"
#echo_log "current git hash: $hash"
echo_log "input arguments:"
echo_log "  run directory: ${CYAN}$run_dir${NC}"
echo_log "  ARTIC gather directory: ${CYAN}$gather_dir${NC}"
echo_log "  threads: ${CYAN}$THREADS${NC}"
echo_log "output files:"
echo_log "  output directory: ${CYAN}$demultiplex_dir${NC}"
echo_log "  log file: ${CYAN}$logfile${NC}"
echo_log "------ beginning analysis ------"

fastq="$gather_dir/${run_base}_fastq_pass.fastq"

artic demultiplex \
	--threads "$THREADS" \
	"$fastq" \
#	2>&1 | while read line; do echo_log "  $line"; done

rm -rf "$workdir"

echo_log "${GREEN}Done "$(basename $0)".${NC}"

#--eof--#
