#!/bin/bash

#---------------------------------------------------------------------------------------------------
# remove line breaks from the sequences in a FASTA file
fastq_rmlinebreaks_4col() {

	input="$*"

	gawk 'BEGIN {
		line = 1;
		seqnum=1;
	} {
		if(NR == 1) {
			printf("%s\t", $0);
			line += 1;
		} else if($0 == "+") {
			line += 2;
		} else if(NR > 1 && (line % 2) != 0) {
			if(substr($0, 1, 1) != "@") {
				printf("\n%s\t%s\n", line, $0);
				exit;
			}
			printf("\n%s\t", $0);
			seq="";
			qual="";
			line += 1;
		} else if((line % 2) == 0 && (line % 4) != 0) {
			seq=sprintf("%s%s", seq, $0);
		} else if((line % 4) == 0) {
			qual=sprintf("%s%s", qual, $0);
			if(length(qual) == length(seq)) {
				printf("%s\t+\t%s\n", seq, qual);
				line += 1;
				seqnum += 1;
			}
		}
	}' "$input"
}

# not a completed script, but hopefully gets the gist across
while getopts "r:b:o:" OPTION
do
	case $OPTION in
		b) BAM=$OPTARG ;;
		r) REFERENCE_FASTA=$OPTARG ;;
		o) OUTPUT_DIR=$OPTARG ;;
		?) usage; exit ;;
	esac
done
# arguments to pass in
# REFERENCE_FASTA=<reference FASTA>
# BAM=<SAM from minimap2 after sam2bam>

# these can be set automatically using the BAM basename
mkdir -p "$OUTPUT_DIR/intermediate_files"
BASE="$(basename ${BAM%.bam})"
OUTPUT_BASE="$OUTPUT_DIR/intermediate_files/$BASE"
VCF="$OUTPUT_BASE.vcf"
AFS_LOG="$OUTPUT_BASE.afs.log"
FASTQ="$OUTPUT_BASE.fq"
FASTA="$OUTPUT_DIR/$BASE.fasta"

# convert BAM to VCF
bcftools mpileup \
	--fasta-ref "$REFERENCE_FASTA" \
	-d 1000000 \
	"$BAM" | bcftools call -c --ploidy 1 > "$VCF" 2> "$AFS_LOG"

# convert VCF to FASTQ
vcfutils.pl vcf2fq \
	-d 10 \
	-D 1000000 \
	"$VCF" > "$FASTQ"

# convert FASTQ to FASTA
fastq_rmlinebreaks_4col "$FASTQ" | \
	awk -F"\t" \
	-v BASE="$BASE" '{
		printf(">%s reference-aligned-consensus-from_%s\n%s\n", BASE, $1, $2)
	}' > "$FASTA"

