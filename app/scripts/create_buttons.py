import pandas as pd; import numpy as np; 
import argparse
import re
import time;
from scipy import stats
import math
from bs4 import BeautifulSoup
parser = argparse.ArgumentParser(description = "Gather proportion of GO Terms that are located in enriched list")
parser.add_argument('-i', required = True, type=str, nargs='+', help = 'Input file (html) to add the tab (scrolling)')
parser.add_argument('-o', required = True, type=str, nargs='+', help = 'output file containing newly edited html file')
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)
args = parser.parse_args()
with open(vars(args)['i'][0], 'r') as file:
    data = file.read().replace('\n', '')
##########Read in the input file
soup = BeautifulSoup(data, 'html.parser')
scripts = []
for elem in soup.findAll(text=re.compile(r'--#!button!--#')):
    scripts.append(elem.string.split("--#")[-1])
    new_tag = soup.new_tag("button", onclick(""))

for e in scripts:
	new_tag = soup.new_tag("script")
	new_tag.string=e
	print(new_tag)
	print(e)
	soup.html.append(new_tag)
# print(soup.prettify())
hk = ''.join(['%s' % x for x in soup.body.findChildren(recursive=False)])




	
with open(vars(args)['o'][0], "w") as file:
    file.write(str(soup.prettify()))