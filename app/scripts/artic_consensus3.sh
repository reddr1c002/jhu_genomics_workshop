#!/bin/bash

# This script is a wrapper for the 'artic minion' consensus generation pipeline

# LICENSE AND DISCLAIMER
#
# Copyright (c) 2019 The Johns Hopkins University/Applied Physics Laboratory
#
# This software was developed at The Johns Hopkins University/Applied Physics Laboratory ("JHU/APL") that is the author thereof under the "work made for hire" provisions of the copyright law. Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation (the "Software"), to use the Software without restriction, including without limitation the rights to copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit others to do so, subject to the following conditions:
#
# 1. This LICENSE AND DISCLAIMER, including the copyright notice, shall be included in all copies of the Software, including copies of substantial portions of the Software;
#
# 2. JHU/APL assumes no obligation to provide support of any kind with regard to the Software. This includes no obligation to provide assistance in using the Software nor to provide updated versions of the Software; and
#
# 3. THE SOFTWARE AND ITS DOCUMENTATION ARE PROVIDED AS IS AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES WHATSOEVER. ALL WARRANTIES INCLUDING, BUT NOT LIMITED TO, PERFORMANCE, MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND NONINFRINGEMENT ARE HEREBY DISCLAIMED. USERS ASSUME THE ENTIRE RISK AND LIABILITY OF USING THE SOFTWARE. USERS ARE ADVISED TO TEST THE SOFTWARE THOROUGHLY BEFORE RELYING ON IT. IN NO EVENT SHALL THE JOHNS HOPKINS UNIVERSITY BE LIABLE FOR ANY DAMAGES WHATSOEVER, INCLUDING, WITHOUT LIMITATION, ANY LOST PROFITS, LOST SAVINGS OR OTHER INCIDENTAL OR CONSEQUENTIAL DAMAGES, ARISING OUT OF THE USE OR INABILITY TO USE THE SOFTWARE.”

#---------------------------------------------------------------------------------------------------
# deal with local environment

# pull nanosecond runtime for temporary file directory
runtime=$(date +"%Y%m%d-%H%M%S-%N")

# identify location of Desktop
DESKTOP="$HOME/Desktop"

# define colors for error messages
red='\033[0;31m'
RED='\033[1;31m'
green='\033[0;32m'
GREEN='\033[1;32m'
yellow='\033[0;33m'
YELLOW='\033[1;33m'
blue='\033[0;34m'
BLUE='\033[1;34m'
purple='\033[0;35m'
PURPLE='\033[1;35m'
cyan='\033[0;36m'
CYAN='\033[1;36m'
NC='\033[0m'

# usage function
usage() {
	echo -e "usage: ${YELLOW}$0${NC} [options]"
	echo -e ""
	echo -e "OPTIONS:"
	echo -e "   -h      show this message"
	echo -e "   -p      run directory (i.e. a directory within ${CYAN}/var/lib/MinKNOW/data${NC})"
	echo -e "   -r      rampart annotation directory"
	echo -e "   -o      output base directory (default: ${CYAN}$DESKTOP/MinION_reports${NC})"
	echo -e "             Note: files will be placed in ${CYAN}<run-directory-base>/artic_consensus${NC} within the directory supplied with -o"
	echo -e "   -s      primer scheme directory (default: ${CYAN}/home/user/bin/rampart-APL/primer_schemes/nCoV-2019/V1${NC})"
	echo -e "   -w      temporary directory (default: ${CYAN}/tmp${NC})"
	echo -e "   -d      threads (default: ${CYAN}$(($(nproc)-1))${NC} on this machine)"
	echo -e ""
}

#---------------------------------------------------------------------------------------------------
# set default values here
output_base="$DESKTOP/MinION_reports"
tempdir="/tmp"
THREADS=$(if [[ $(nproc) -gt 1 ]]; then echo $(($(nproc)-1)); else echo 1; fi)
scheme_dir="/home/user/bin/rampart-APL/primer_schemes/nCoV-2019/V1/"

# parse input arguments
while getopts "hp:r:o:s:l:w:d:" OPTION
do
	case $OPTION in
		h) usage; exit 1 ;;
		p) run_dir=$OPTARG ;;
		r) annotation_dir=$OPTARG ;;
		o) output_dir=$OPTARG ;;
		l) logfile=$OPTARG ;;
		s) scheme_dir=$OPTARG ;;
		w) tempdir=$OPTARG ;;
		d) THREADS=$OPTARG ;;
		?) usage; exit 1 ;;
	esac
done

#---------------------------------------------------------------------------------------------------
# error checking

# make sure run directory exists
if ! [[ -d "$run_dir" ]]; then
	echo -e "\n${RED}Error: specified run directory \"$run_dir\" does not exist.${NC}\n" >&2
	usage
	exit 2
fi

# make sure run directory exists
if ! [[ -d "$scheme_dir" ]]; then
	echo -e "\n${RED}Error: specified primer scheme directory \"$scheme_dir\" does not exist.${NC}\n" >&2
	usage
	exit 2
fi

# grab information about run
run_dir="${run_dir%/}"
fastq_dir=$(find "$run_dir" -regextype posix-extended -regex ".*/fastq[_/]pass/.*.fastq" -printf "%h\n" | head -n1)
run_base=$(basename "$run_dir")

base_dir="$output_base/$run_base"

# set up output directory for porechop directories and individual kraken files
demultiplex_dir="$base_dir/artic_demultiplex"
mkdir -p "$demultiplex_dir"

# make sure RAMPART directory exists
if ! [[ -d "$annotation_dir" ]]; then
	annotation_dir="$base_dir/annotations"
	if ! [[ -d "$annotation_dir" ]]; then
		echo -e "\n${RED}Error: specified RAMPART annotation file directory \"$annotation_dir\" does not exist.${NC}\n" >&2
		usage
		exit 2
	fi
fi
#---------------------------------------------------------------------------------------------------

# grab protocol info
scheme_dir="${scheme_dir%/}"
protocol=$(echo "$scheme_dir" | rev | cut -d"/" -f1-2 | rev)
scheme_dir="${scheme_dir%$protocol}"

# set up output directory for porechop directories and individual kraken files
consensus_dir="$base_dir/artic_consensus"
mkdir -p "$consensus_dir"

# set up working directory within temp directory
workdir="$tempdir/artic_consensus-$runtime"
mkdir -p "$workdir"


# define default logfile
if [[ -z "$logfile" ]]; then
	logfile="$consensus_dir/artic_consensus.log"
fi

#---------------------------------------------------------------------------------------------------
# dependency checking

artic_check=$(artic minion -h 2> /dev/null | wc -l)
if [[ "$artic_check" -eq 0 ]]; then
	echo -e "\n${RED}Error: ${YELLOW}artic minion${RED} is not present in this environment.${NC}\n" >&2
	echo -e "\n${RED}       Make sure you have activated the right conda environment with ${YELLOW}conda activate env_name${RED}.${NC}\n" >&2
	exit 2
fi

#===================================================================================================
# defining functions
#===================================================================================================

echo_log() {

	input="$*"

	# if input is non-empty string, prepend initial space
	if [[ -n "$input" ]]; then
		input=" $input"
	fi

	# print to STDOUT
	echo -e "[$(date +"%F %T")]$input"

	# print to log file (after removing color strings)
	echo -e "[$(date +"%F %T")]$input\r" | sed -r 's/\x1b\[[0-9;]*m?//g' >> "$logfile"
}

fix_fasta() {

	fasta="$1"

	sed 's/\r//g' "$fasta" | \
	awk 'BEGIN{ FS="\t"; } {
		if(NR == 1) {
			printf("%s\n", $0);
		} else {
			if(substr($0,1,1) == ">") {
				printf("\n%s\n", $0);
			} else {
				printf("%s", $0);
			}
		}
	} END {	printf("\n"); }' | \
	sed '$!N;s/\n/\t/' | awk -F$'\t' '{printf("%s\n%s\n", $1, toupper($2));}'
}

mutations() {

	seq1="$1"
	seq2="$2"

	if [[ "${#seq1}" -ne "${#seq2}" ]]; then
		echo "Error: sequences must be the same length ("${#seq1}" != "${#seq2}")"
		exit
	fi

	printf "%s\t%s\n" "$seq1" "$seq2" | awk -F $'\t' '{
		for(i=1; i<=length($1); i++) {
			q1=substr($1,i,1);
			q2=substr($2,i,1);
			if(q1 != q2) {
				printf("%s%s%s\n", q1, i, q2);
			}
		}
	}'
}

#===================================================================================================
# Main body
#===================================================================================================

#===================================================================================================

# report current hash of git repo
#GIT_DIR="$(dirname $(readlink -f $(which $(basename $0))))/.git"
#export GIT_DIR
#hash=$(git rev-parse --short HEAD)

# set up log file
echo_log "------ Call to "${YELLOW}$(basename $0)${NC}" from "${GREEN}$(hostname)${NC}" ------"
#echo_log "current git hash: $hash"
echo_log "input arguments:"
echo_log "  run directory: ${CYAN}$run_dir${NC}"
echo_log "  RAMPART annotation directory: ${CYAN}$annotation_dir${NC}"
echo_log "  scheme directory: ${CYAN}$scheme_dir${NC}"
echo_log "  protocol: ${CYAN}$protocol${NC}"
echo_log "output files:"
echo_log "  demultiplex directory: ${CYAN}$demultiplex_dir${NC}"
echo_log "  output directory: ${CYAN}$consensus_dir${NC}"
echo_log "  log file: ${CYAN}$logfile${NC}"
echo_log "------ beginning analysis ------"

echo_log "removing existing demultiplexed files"
find "$demultiplex_dir" -name "*.fastq" -print0 | while read -d $'\0' output_file; do
	rm "$output_file"
done

echo_log "parsing RAMPART output to create demultiplexed FASTQ files"
find "$fastq_dir" -name "*.fastq" -print0 | sort -z | while read -d $'\0' fastq_file; do

	annotation_file="$annotation_dir"/$(basename "${fastq_file%.fastq}").csv

	if [[ -s "$annotation_file" ]]; then

		gawk -v OUTPUT_DIR="$demultiplex_dir" '{
			if(NR==FNR) {
				barcode[$1] = $4;
				fix_ref = gensub(/[|]/, "_", "g", $5);
				switch(fix_ref) {
					case "*": fix_ref = "unaligned"; break;
					case "?": fix_ref = "unknown"; break;
				}
				reference[$1] = fix_ref;
			} else {
				split($1, a, " ");
				query=substr(a[1],2);
				if(query in barcode) {
					filename=sprintf("%s/%s-%s.fastq", OUTPUT_DIR, barcode[query], reference[query]);
					printf("%s\n%s\n%s\n%s\n", $1, $2, $3, $4) >> filename;
				}
			}
		}' FS="," <(tail -n+2 "$annotation_file") FS=$'\t' <(sed '$!N;s/\n/\t/' "$fastq_file" | sed '$!N;s/\n/\t/')
	fi
done

echo_log "generating consensus sequences from demultiplexed FASTQ files"
find "$demultiplex_dir" -name "*.fastq" ! -name "*unknown*" ! -name "*unaligned*" | while read fastq_file; do

	fastq_base=$(basename "$fastq_file")

	artic minion \
		--medaka \
		--minimap2 \
		--normalise 200 \
		--threads "$THREADS" \
		--scheme-directory "$scheme_dir" \
		--read-file "$fastq_file" \
		"$protocol" \
		"$consensus_dir/$fastq_base" \
#		2>&1 | while read line; do echo_log "  $line"; done

done

outputJSON="$workdir/output.json"
printf "{\n" > "$outputJSON"

# count total consensus files
num_consensus_files=$(find "$consensus_dir" -name "*.consensus.fasta" -print0 | tr -d -c "\000" | wc -c)

# add each consensus file to JSON output
count=0
find "$consensus_dir" -name "*.consensus.fasta" -print0 | while read -d $'\0' consensus_file; do

	count=$(($count+1))

	# grab associated files
	reference_file=$(find "$scheme_dir/$protocol" -name "*.reference.fasta")
	reference_sequence=$(fix_fasta "$reference_file" | tail -n1)
	fastq_file="$demultiplex_dir"/$(basename "${consensus_file%.consensus.fasta}")

	# pull out metadata to print
	barcode=$(basename "$consensus_file" | cut -d"-" -f1)
	reference_header=$(basename "$consensus_file" | cut -d"-" -f2-)
	reference_acc=$(head -n1 "$consensus_file" | rev | cut -d"-" -f1 | rev)
	reference_length=$(echo "$reference_sequence" | awk '{print length($1)}')
	timestamp=$(stat -c "%y" "$consensus_file" | cut -c1-19)
	number_of_reads=$(($(wc -l < "$fastq_file")/4))
	ambiguous_nucleotides=$(tail -n1 "$consensus_file" | grep -o "N" | wc -l)
	unambiguous_nucleotides=$(($reference_length - $ambiguous_nucleotides))
	mutations=$(mutations "$reference_sequence" $(tail -n1 "$consensus_file") | grep -v N)
	sequence=$(tail -n1 "$consensus_file")

	# create JSON
	printf "\"entry%s\": {\n" "$count" >> "$outputJSON"
	printf "\"barcode\": \"%s\",\n" "$barcode" >> "$outputJSON"
	printf "\"reference_header\": \"%s\",\n" "$reference_header" >> "$outputJSON"
	printf "\"reference_acc\": \"%s\",\n" "$reference_acc" >> "$outputJSON"
	printf "\"reference_length\": %s,\n" "$reference_length" >> "$outputJSON"
	printf "\"timestamp\": \"%s\",\n" "$timestamp" >> "$outputJSON"
	printf "\"number_of_reads\": %s,\n" "$number_of_reads" >> "$outputJSON"
	printf "\"unambiguous_nucleotides\": %s,\n" "$unambiguous_nucleotides" >> "$outputJSON"
	printf "\"mutations\": [" >> "$outputJSON"
	if [[ -n "$mutations" ]]; then
		printf "%s\n" "$mutations"
		echo "$mutations" | awk -F $'\t' 'BEGIN{out=""}{
				out=sprintf("%s,\"%s\"", out, $0);
		} END {
			printf("%s", substr(out, 2));
		}' >> "$outputJSON"
	fi
	printf "],\n" >> "$outputJSON"
	printf "\"sequence\": \"%s\"\n}" "$sequence" >> "$outputJSON"

	if [[ "$count" -lt "$num_consensus_files" ]]; then
		printf ",\n" >> "$outputJSON"
	else
		printf "\n" >> "$outputJSON"
	fi
done

printf "}\n" >> "$outputJSON"

cp "$outputJSON" "$consensus_dir/consensus_report.json"

rm -rf "$workdir"

echo_log "${GREEN}Done "$(basename $0)".${NC}"

#--eof--#
