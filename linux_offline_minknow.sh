#!/bin/bash

MINKNOW_ROOT=/opt/ont/minknow/

sudo ${MINKNOW_ROOT}/bin/config_editor --filename ${MINKNOW_ROOT}/conf/sys_conf --conf system --set on_acquisition_ping_failure=ignore

echo ${MINKNOW_ROOT}/conf/sys_conf

file ${MINKNOW_ROOT}/conf/sys_conf

SET_AS=`${MINKNOW_ROOT}/bin/config_editor --filename ${MINKNOW_ROOT}/conf/sys_conf --conf system --get_all | grep on_acquisition_ping_failure`

echo "Ping status: " ${SET_AS} 

