script_location="$(perl -MCwd=abs_path -le 'print abs_path(shift)' $(which $(basename $0)))"
# Software_location="$HOME/Documents/Software/"

binpath=$HOME/bin/

LAUNCHER_dir="$HOME/.local/share/applications"
kraken_database_location="$HOME/Documents/database/flukraken-2019-06-24"
#use this command inside of app to do a quick run through
#cd ../Install_Scripts/ && bash quick_conv.sh  && cd ../app && electron . 
# create launcher and add to favorites
#sed "s@<SCRIPT_LOCATION>@$script_location/../@g" "$script_location/Workshop.desktop" > "$LAUNCHER_dir/jhu_genomics_workshop.desktop"
#dconf write /org/gnome/shell/favorite-apps "['firefox.desktop', 'org.gnome.Nautilus.desktop', 'libreoffice-writer.desktop', 'org.gnome.Software.desktop', 'jhu_genomics_workshop.desktop']"

# add correct path variables to renderer.js
sed -e "s@<binpath>@$kraken_database_location@g" \
    -e "s@<APP_DIR>@$script_location/../app@g" \
	"$script_location/renderer.js" > "$script_location/../app/renderer.js"

# add correct path variables to main.js
sed -e "s@<APP_DIR>@$script_location/../app@g" \
	"$script_location/main.js" > "$script_location/../app/main.js"
