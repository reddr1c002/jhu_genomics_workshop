/* WARNING:

The base version of this script is found in:

	jhu_genomics_workshop/Install_Scripts/renderer.js

That script is modified for environment variables and copied to:

	jhu_genomics_workshop/app/renderer.js

All edits should be made in the first script (in Install_Scripts).

 */

// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.
const {promisify} = require('util')
const homedir = require("os").homedir();
const remote = require('electron').remote
const mkdirp = require('mkdirp');
const spawn = require('child_process').spawn;
var http = require('http')

const readFile = promisify(fs.readFile)
var dat_loc = "<datpath>"
var generate_dat = ""
var orig = dat_loc
const options = {
    type: 'warning',
    buttons: ['OK'],
    defaultId: 2,
    title: 'Alert there is an error',
    message: '',
  };
///////////////Check if table files are present and override indexhtml elements if so
var old_json = ""

// Functions!!////////////////////////////////////////
function show_process(){
  const BrowserWindow = remote.BrowserWindow;
  const win2 = new BrowserWindow({ width: 500, height: 400, x: 800,
        y: 0 })
  win2.loadFile('<APP_DIR>/tabs/fastq/fastq3.html')
  win2.webContents.openDevTools()
	win2.webContents.on("did-finish-load", () =>{
	})
}

function openRAMPART(){
	console.log("opening rampart")
	shell.openExternal('http://localhost:3000')
}

//Rampart Section /////////////////////////////////////
$('#rampart').on('shown.bs.collapse', function () {
	var folder = document.getElementById("myInput")
	if(!(folder.files)){
		const { dialog } = require('electron').remote
		options.message ="No run directory specified. Please choose a run directory in the left-hand panel and run rampart in this window."
		dialog.showMessageBox(null, options, (response) => {
		});
	}
})
$('#startRampart').on("click",function(){
	var folder = document.getElementById("myInput").files
	var protocols = document.getElementById("protocol_files").files
	var outputFolder = ''
	if (protocols == undefined){
		protocols = "<APP_DIR>/../src/rampart_jhuapl/protocols/ncov2019"
	}
//	if (folder == undefined){
		outputFolder = "<OUTPUT_DIR>/" + path.basename(folder)
//	}else{
//		outputFolder = folder
//	}

	let command = "gnome-terminal --command=\"/bin/bash -c 'source <ENV_LOC>/etc/profile.d/conda.sh " +
	" && mkdir -p "+outputFolder+" && cd "+ outputFolder +
	"  && conda deactivate && conda activate artic-rampart-apl && rampart "+
	" --basecalledPath $(find \"" + folder+"\" -regextype posix-extended -regex \"\.*/fastq[_/]pass/\.*\.fastq\" -printf \"%h\\n\" | head -n1) " +
	" --protocol " + protocols + ";  exec bash'\""
	let child = exec(command)
	// $('#closeRampart').show()
	// $('#startRampart').hide()
	$('#closeRampart').on("click", function(){
		child.stdin.pause()
		child.kill('SIGKILL')
		$('#startRampart').show()
	})
	setTimeout(function(){  console.log("Loading rampart"), $('#rampartObjDiv').load(document.URL +  ' #rampartObj')}, 7000);

})



$('#protocol_files').on("click", function(){
	const { dialog } = require('electron').remote
	dialog.showOpenDialog({
        properties: ['openDirectory']
    }).then((dir)=>{
		if (dir.filePaths !== undefined && dir.filePaths.length > 0) {
			$('#startRampart').show()
			$("#negativeFirstStep").css("background-color", "green")
			$("#protocolDirectoryText").text(dir.filePaths[0])
			document.getElementById("protocol_files").files = dir.filePaths[0]
	    }
	})
})

//Data location by default for mytax and beyond
dat_loc = "<datpath>/flukraken-2019-11-01"

$('#flukraken:checkbox').on("change", function(){
    if ($(this).is(':checked')) {
        dat_loc = "<datpath>/flukraken-2019-11-01"
    }
    else{
    	dat_loc = orig
    }
});

//Input file for process oxford (contains MinION Reads)
$('#myInput').on("click", function(){
	const { dialog } = require('electron').remote
	dialog.showOpenDialog({
        properties: ['openDirectory']
    }).then((dir)=>{
		if (dir.filePaths !== undefined && dir.filePaths.length > 0) {
			$('#protocol_files').show()
			$('#rampartInit').show()
			let folderBase =dir.filePaths[0].substr(dir.filePaths[0].lastIndexOf("/") + 1);
			let path = '<OUTPUT_DIR>/'+folderBase
			artic_consensus_watch(path)

			$("#negativeFirstStep").css("background-color", "green")
			$("#runDirectoryText").text(dir.filePaths[0])
			document.getElementById("myInput").files = dir.filePaths[0]
			fs.readFile(dir.filePaths[0]+"/full.log",(err,data)=>{
				data = data.toString().split(String.fromCharCode(10));
				var h6 = document.createElement("h6");
				data.forEach(function(d){
					h6.innerHTML += d+"<br>\n"
				})
				$('#logDiv h6').remove()
				$('#logDiv').append(h6);
			})
	    }
	})
})
//Process oxford run on MinION Reads
function run_process_oxford(cmd){
	const { dialog } = require('electron').remote
	var folder = document.getElementById("myInput")
	if(!(folder.files)){
		options.message ="No run directory specified. Please choose a base directory that contains the fastq_pass folder."
		$("#cancel_group").hide()
		dialog.showMessageBox(null, options, (response) => {
		});
	}
	else{
		const dir = "."
		if (cmd !="auto"){
			$("#zeroStep").css("background-color", "goldenrod")
			let command ="";

			if($('#Workshop_mode').is(':checked')){
				command = "gnome-terminal --command=\"/bin/bash -c '<APP_DIR>/pre-commands/minion_commands.sh "
				var command_fill = command.concat(" <APP_DIR>/scripts/process_oxford.sh -p "+folder.files+" -k "+dat_loc+"; exec bash'\"")
				command=command_fill
			}
			else{
				command = "gnome-terminal --command=\"/bin/bash -c 'source <ENV_LOC>/etc/profile.d/conda.sh &&  conda activate jhu_workshop && <APP_DIR>/scripts/process_oxford.sh -p "+folder.files+" -k "+dat_loc+"; exec bash'\""
			}
			execSync(command)
			$("#zeroStep").css("background-color", "green")
		}
		else{
			processOxfordPromise(folder.files).then(()=>{
				$("#zeroStep").css("background-color", "green")
				
				splitPromise(folder.files).then(()=>{
					$("#firstStep").css("background-color", "green")
					
					referenceGenerationPromise(folder.files).then(()=>{
						
						$("#secondStep").css("background-color", "green")
						referenceAlignmentPromise(folder.files).then(()=>{
							
							$("#thirdStep").css("background-color", "green")

							makeConsensusPromise(folder.files).then(()=>{
								
								$("#fourthStep").css("background-color", "green")
								$("#consensusStep").css("background-color", "green")

								const { dialog } = require('electron').remote
									options.message ="Completed your full run. Consensus sequnces are generated for all segments"
									dialog.showMessageBox(null, options, (response) => {});
							})
						})
					})
				})
			})
		}
	}
}
log_string=""

function processOxfordPromise(folderFiles){
	folder={}
	folder['files']= folderFiles
	let children=[]
	$("#zeroStep").css("background-color", "goldenrod")
	fs.stat(folder.files+"/output/1-"+path.basename(dat_loc)+"/done.log",function(err,stat){
		if(err==null){
			fs.unlinkSync(folder.files+"/output/1-"+path.basename(dat_loc)+"/done.log",function(err){
				if (err) ;

			})
		}				

	})
	fs.stat(folder.files+"/output/full.log",function(err,stat){
		if(err==null){
			fs.unlinkSync(folder.files+"/output/full.log",function(err){
				if (err) ;
			})
		}
	})
	return new Promise((resolve,reject)=>{
		let command_fill = "bash <APP_DIR>/scripts/process_oxford.sh -p "+folder.files+" -k "+dat_loc +" -l "+folder.files+"/output/1-"+path.basename(dat_loc)+"/process.log"
		+" && touch "+folder.files+"/output/1-"+path.basename(dat_loc)+"/done.log"
		if (!fs.existsSync(folder.files+"/output/0-porechop")){
			fs.mkdirSync(folder.files+"/output/")
			fs.mkdirSync(folder.files+"/output/0-porechop")
		}
		if (!fs.existsSync(folder.files+"/output/1-"+path.basename(dat_loc))){
			fs.mkdirSync(folder.files+"/output/1-"+path.basename(dat_loc))
		}
		let child = exec(command_fill)
		children.push(child)
		waitFileComplete(folder.files+"/output/1-"+path.basename(dat_loc)+"/done.log",folder.files,folder.files+"/output/1-"+path.basename(dat_loc)+"/process.log")
		.then(()=>{
			
			resolve()
		})
		$("#cancel_group").on("click",function(d){
		  	// 
			const { dialog } = require('electron').remote
			// children.forEach(function(child){
				child.stdin.pause()
				child.kill('SIGKILL')				
			// })
			// process.kill(-child.pid)
			options.message ="Canceling the Job(s). This could take some time depending on the step in the process"
				dialog.showMessageBox(null, options, (response) => {
			});
			$("#cancel_group").hide()
			$("#zeroStep").css("background-color", "firebrick")
			reject()
		}) 
	})
}
var deleteFolderRecursive = function(path) {
  if( fs.existsSync(path) ) {
    fs.readdirSync(path).forEach(function(file,index){
      var curPath = path + "/" + file;
      if(fs.lstatSync(curPath).isDirectory()) { // recurse
        deleteFolderRecursive(curPath);
      } else { // delete file
        fs.unlinkSync(curPath,function(error){

        });
      }
    });
    fs.rmdirSync(path);
    options.message ="Deleted files at: "+path
  }
  else{
  	options.message ="No output dir found in this path: "+path	
  }
  return options.message
};
$("#delete_runFolder").on("click",function(){
	folder = document.getElementById("myInput")
	const { dialog } = require('electron').remote
	
	if (!(folder.files)){
		options.message ="No directory selected to delete output folder"
		dialog.showMessageBox(null, options, (response) => {
		});
	}
	else{
		options.message = deleteFolderRecursive(folder.files+"/output")
		dialog.showMessageBox(null, options, (response) => {});
	}
})

function makeConsensusPromise(folderFiles){
	if (!fs.existsSync(folder.files + "/output/5-consensus-sequences")){
		fs.mkdirSync(folder.files + "/output/5-consensus-sequences")
	}
	$("#fourthStep").css("background-color", "goldenrod")
	var children=[];
	return new Promise((resolve2, reject) => {
		folder={}
		folder['files']=folderFiles
		fs.readdir(folder.files+"/output/4-reference_alignment/", (err, files) => {
		  var promises = [];
	  	  for (let i =0; i< files.length; i++ ) {
	  	  	file= files[i]
			if(file.indexOf(".sam") != -1){
				filebase = file.replace(/-.+$/, "")
				fileExt = file.replace(/.fastq$/,"")
				alignment_file = file
				var bam_file = alignment_file.replace(/\.sam/, '.bam')
				var command = "<APP_DIR>/scripts/sam2bam.sh "
				+folder.files+"/output/4-reference_alignment/"+alignment_file+" && <APP_DIR>/scripts/bam2fasta.sh -r "+folder.files+"/output/3-closest-reference/"+filebase+"-closest-reference-IAV.fasta"+
				"  -b "+folder.files+"/output/4-reference_alignment/"+bam_file
				+" -o "+folder.files+"/output/5-consensus-sequences/ && touch "+folder.files + "/output/5-consensus-sequences/"+fileExt+"_done.log"
				promises.push(waitFileComplete(folder.files + "/output/5-consensus-sequences/"+fileExt+"_done.log",folder.files))
				let child  = exec(command)
				children.push(child)
			}
			};
			
			Promise.all(promises).then(()=>{resolve2()})
		})
		$("#cancel_group").on("click",function(d){
		  	// 
			const { dialog } = require('electron').remote
			children.forEach(function(child){
				child.stdin.pause()
				child.kill('SIGKILL')
			})
			options.message ="Canceling the Job"
			dialog.showMessageBox(null, options, (response) => {
			});
			$("#cancel_group").hide()
			$("#fourthStep").css("background-color", "firebrick")

			reject()
	  	}) 
	})
	
}
function referenceAlignmentPromise(folderFiles){
	if (!fs.existsSync(folder.files + "/output/4-reference_alignment")){
		fs.mkdirSync(folder.files + "/output/4-reference_alignment")
	}
	$("#thirdStep").css("background-color", "goldenrod")

	return new Promise((resolve2, reject) => {
		folder={}
		folder['files']=folderFiles
		var children=[];
		fs.readdir(folder.files+"/output", (err, splitFile) => {
		  cont=0; true_files=[];
		  splitFile.forEach(function(d){
		  	if (d.indexOf(".fastq") != -1){
		  		true_files.push(d)
		  	}
		  })
		  var promises = [];
		  var files=splitFile;
	  	  for (let i =0, p=Promise.resolve(); i< files.length; i++ ) {
		  	if(files[i].indexOf(".fastq") != -1){
		  		promises.push(p);
		  		p=p.then(_=> new Promise(resolve3=>{
			  		filebaseTop = files[i]
			  		filebaseTop=filebaseTop.replace(/\.fastq+$/, "")
			  		var promises2=[];
					fs.readdir(folder.files+"/output/2-split-level/"+filebaseTop, (err, files2) => {
						files2.forEach(file => {
						  	if(file.indexOf(".fastq") != -1){
						  		filebase = file.replace(/\.fastq+$/, "")
						  		filebaseTop = filebase.replace(/-.+$/,"")
								var output_file = folder.files + "/output/4-reference_alignment/"+ filebase
								var command = "minimap2 -x map-ont -a -o "+output_file
								+".sam --cs \""+folder.files+"/output/3-closest-reference/"+filebaseTop+"-closest-reference-IAV.fasta"
								+"\" \""+folder.files+"/output/2-split-level/"+filebaseTop+"/"+file
								+"\" 2> "+output_file+".minimap2.log && touch "+folder.files +"/output/4-reference_alignment/"+filebase+"_done.log"
								// 
								promises2.push(waitFileComplete(folder.files +"/output/4-reference_alignment/"+filebase+"_done.log",folder.files,output_file+".minimap2.log"))
								let child  = exec(command)
								children.push(child)
							}
						});
			  			Promise.all(promises2).then(()=>{ resolve3()})
					})
				}))
			}
		  }
		  
		  Promise.all(promises).then(()=>{ resolve2()})

		}) 
		$("#cancel_group").on("click",function(d){
			const { dialog } = require('electron').remote
			children.forEach(function(child){
				child.stdin.pause()
				child.kill('SIGKILL')
			})
			options.message ="Canceling the Job"
			dialog.showMessageBox(null, options, (response) => {
			});
			$("#cancel_group").hide()
			$("#thirdStep").css("background-color", "firebrick")
			reject()
		}) 
	})
}
function referenceGenerationPromise(folderFiles){
	if (!fs.existsSync(folder.files + "/output/3-closest-reference")){
		fs.mkdirSync(folder.files + "/output/3-closest-reference")
	}
	var children=[];
	$("#consensusStep").css("background-color", "goldenrod")
	$("#secondStep").css("background-color", "goldenrod")
	return new Promise((resolve2, reject) => {
		folder={}
		folder['files']=folderFiles
		fs.readdir(folder.files+"/output", (err, files) => {
		  cont=0; true_files=[];
		  files.forEach(function(d){
		  	if (d.indexOf(".fastq") != -1){
		  		true_files.push(d)
		  	}
		  })
		  var promises = []; 
	  	  for (let i =0;  i< files.length; i++ ) {
	  	  	fileBase= files[i]	
	  	  	if(fileBase.indexOf(".fastq") != -1){
  				file = files[i]
		  		filebase = file.replace(/\.fastq+$/, "")
				kraken_file = folderFiles+"/output/1-"+path.basename(dat_loc)+"/"+filebase+".kraken.report.full"
				var command= "bash <APP_DIR>/scripts/get_closest_reference.sh"
				+" -i "+kraken_file
				+" -k "+dat_loc
				+" -o "+folder.files + "/output/3-closest-reference/"+filebase+"-closest-reference.fasta && touch "+folder.files + "/output/3-closest-reference/"+filebase+"_done.log";
				let child = exec(command)
				children.push(child)
				promises.push(waitFileComplete(folder.files + "/output/3-closest-reference/"+filebase+"_done.log","yes"))
			}
		  };
		  
		  Promise.all(promises).then(()=>{resolve2()})
		})
		$("#cancel_group").on("click",function(d){
			const { dialog } = require('electron').remote
			children.forEach(function(child){
				child.stdin.pause()
				child.kill('SIGKILL')
			})
			options.message ="Canceling the Job"
			dialog.showMessageBox(null, options, (response) => {
			});
			$("#cancel_group").hide()
			$("#secondStep").css("background-color", "firebrick")

			reject()
		 })
	})
}

function splitPromise(folderFiles){
	if (!fs.existsSync(folder.files + "/output/2-split-level")){
		fs.mkdirSync(folder.files + "/output/2-split-level")
	}
	$("#firstStep").css("background-color", "goldenrod")
	return new Promise((resolve2, reject) => {
		folder={}
		folder['files']=folderFiles
		fs.readdir(folder.files+"/output", (err, files) => {
		  cont=0; true_files=[];
		  files.forEach(function(d){
		  	if (d.indexOf(".fastq") != -1){
		  		true_files.push(d)
		  	}
		  })
		  var promises = []
		  var children=[]
	  	  for (let i =0, p= Promise.resolve();  i< files.length; i++ ) {
		  	if(files[i].indexOf(".fastq") != -1){
		  		promises.push(p)
		  		p=p.then(_=> new Promise(resolve3=>{
			  			filebase = files[i].replace(/\.fastq+$/, "")
					    var command="<APP_DIR>/scripts/split_segments.sh -f "+folder.files+"/output/"+files[i]
						+" -r "+folder.files+"/output/1-"+path.basename(dat_loc)+"/"+filebase+".kraken.report.full"
						+" -k "+folder.files+"/output/1-"+path.basename(dat_loc)+"/"+filebase+".kraken"
						+" -o "+folder.files+"/output/2-split-level/"
						+filebase+"/ && touch "+folder.files+"/output/2-split-level/"+filebase+"/done.log" 
						let child = exec(command)
						children.push(child)
						dir=folder.files+"/output/2-split-level/"+filebase
						if (!fs.existsSync(dir)){
						    fs.mkdirSync(dir);
						}
						waitFileComplete(folder.files+"/output/2-split-level/"+filebase+"/done.log",folder.files).then(()=>{resolve3()})
					}));
				}
			};
		  Promise.all(promises).then(()=>{ resolve2()})
		})
	})
}

	



function appendLogFile(filenameLog,masterLog){
	fs.stat(filenameLog,function(err,stat){
		if(err==null){
			// 
			fs.copyFile(filenameLog, masterLog, (err2,data)=> {
			  if (err2) throw err2;
				 fs.readFile(masterLog,"utf8",(err3,data3)=>{
				 	if (err3) throw err;
				 	data3 = data3.toString().split(String.fromCharCode(10));
					var h6 = document.createElement("h6");
					data3.forEach(function(d){
						h6.innerHTML += d+"<br>\n"
					})
					$('#logDiv h6').remove()
					$('#logDiv').append(h6);
					})
			});
			

		}
	})


}

function waitFileComplete(filename,base,log) {
    return new Promise(function (resolve, reject) {
        fs.access(filename, fs.constants.R_OK, function (err) {
            if (!err) {
                watcher.close();
                resolve();
            }
        });
        if(log != null){
        	fs.watchFile(log, (curr, prev) => {
        		appendLogFile(log, base+"/full.log")
			});
        }
        var dir = path.dirname(filename);
        var basename = path.basename(filename);
        var watcher = fs.watch(dir, function (eventType, filename) {
            if (eventType === 'rename' && filename === basename) {
                resolve();
            }
        });
    });
}

//Find the database location and process it with process_krakendb.sh
$('#database_loc_process').on("click", function(){
	if (orig != dat_loc){
		const command = "gnome-terminal --command=\"/bin/bash -c 'source <ENV_LOC>/etc/profile.d/conda.sh &&  conda activate jhu_workshop && process_krakendb.sh -k "+dat_loc+"; exec bash'\""
		exec(command)
	}
})
$('#database_loc').on("click", function(){
	const { dialog } = require('electron').remote
	 dialog.showOpenDialog({
        properties: ['openDirectory'],
        defaultPath:"<datpath>"
    }).then((dir)=>{
    	if (dir !== undefined) {
            document.getElementById("database_loc").files = dir.filePaths[0]
            dat_loc = dir.filePaths[0]
        }	
    }); 
})
$('#build_flukraken_run').on("click", function(){
	generate_dat = "<datpath>"
	if (generate_dat == "" || generate_dat == undefined){
		const { dialog } = require('electron').remote
		options.message ="No location or invalid location selected to generate database in. Please choose a valid folder/location."
			dialog.showMessageBox(null, options, (response) => {
		});
	}
	else{
		
		const command = "gnome-terminal --command=\"/bin/bash -c ' source <ENV_LOC>/etc/profile.d/conda.sh &&  conda activate jhu_workshop && build_flukraken.sh -k "+generate_dat+"; exec bash'\""
		exec(command)
	}
})

function make_consensus(ref_file, type){
	// 
	$("#fourthStep").css("background-color", "goldenrod")
	if(type=="single"){
		var folder = document.getElementById("myInput")
		mkdirp(folder.files + "/output/5-consensus-sequences", function(err) { 
			if(err){
				const { dialog } = require('electron').remote
				options.message ="Error in making the directory: "
				+folder.files + "/output/5-consensus-sequences, Try checking permissions or making it yourself"
				"Error: "+err
				dialog.showMessageBox(null, options, (response) => {
				}); $("#fourthStep").css("background-color", "firebrick")
			}
			else{
				if($('#Workshop_mode').is(':checked')){
					var ref_file = document.getElementById("reference_file").files[0].path
					var alignment_file = document.getElementById("alignment_file").files[0].path
					var bam_file = alignment_file.replace(/\.sam/, '.bam')
					var alignment_base = alignment_file.substr(alignment_file.lastIndexOf("\/")+1);
					var output_path = folder.files + "/output/5-consensus-sequences"
					var command = "gnome-terminal --command=\"/bin/bash -c '<APP_DIR>/pre-commands/bam_tofasta.sh "
					+alignment_file+" "+ref_file+" "+bam_file
					+" "+output_path
					+" <APP_DIR>/scripts/sam2bam.sh <APP_DIR>/scripts/bam2fasta.sh; exec bash'\""
					execSync(command)
				}
				else{
					var ref_file = document.getElementById("reference_file").files[0].path
					var alignment_file = document.getElementById("alignment_file").files[0].path
					var bam_file = alignment_file.replace(/\.sam/, '.bam')
					var alignment_base = alignment_file.substr(alignment_file.lastIndexOf("\/")+1);
					var output_path = folder.files + "/output/5-consensus-sequences"
					var command = "gnome-terminal --command=\"/bin/bash -c 'source <ENV_LOC>/etc/profile.d/conda.sh &&  conda activate jhu_workshop && <APP_DIR>/scripts/sam2bam.sh "
					+alignment_file+" && <APP_DIR>/scripts/bam2fasta.sh \\\n\t -r "+ref_file+
					" \\\n\t -b "+bam_file
					+" -o "+output_path+"; exec bash'\""
					execSync(command)
				}
				$("#fourthStep").css("background-color", "green")
				$("#consensusStep").css("background-color", "green")
			}
		 })
	}
	else{
		
	}
	
}


function split_level(fastq_file,type){
	var report_file = document.getElementById("report_file")
	var kraken_file = document.getElementById("kraken_file")
	const { dialog } = require('electron').remote
	$("#firstStep").css("background-color", "goldenrod")
	if(type=="single"){
		var folder = document.getElementById("myInput")
		if (!(kraken_file)){
			options.message ="No kraken file chosen. Please select one from the output directory of the MinION read run (e.g. BC01.kraken) "
			dialog.showMessageBox(null, options, (response) => {
			});	$("#firstStep").css("background-color", "firebrick")
		}
		else if (!(fastq_file)){
			options.message ="No fastq file chosen. Please select one from the output directory of the MinION read run (e.g. BC01.fastq) "
			dialog.showMessageBox(null, options, (response) => {
			});	$("#firstStep").css("background-color", "firebrick")

		}
		else if (!(report_file)){
			options.message ="No full string kraken report file chosen. Please select one from the output directory of the MinION read run (e.g. BC01.kraken.report.full) "
			dialog.showMessageBox(null, options, (response) => {
			});
			$("#firstStep").css("background-color", "firebrick")

		}
		else{
			
			if(!(folder.files)){
				const { dialog } = require('electron').remote
				options.message ="No run directory specified. Please choose a base directory that contains the fastq_pass folder."
				dialog.showMessageBox(null, options, (response) => {
				});
			} else{
				mkdirp(folder.files + "/output/2-split-level", function(err) { 
					if(err){
						const { dialog } = require('electron').remote
						options.message ="Error in making the directory: "+folder.files + "/output/2-split-level, Try checking permissions or making it yourself"
						+"Error: "+err
						dialog.showMessageBox(null, options, (response) => {
						});
					}
					else{
						var command =""
						if($('#Workshop_mode').is(':checked')){
							var command = "gnome-terminal --command=\"/bin/bash -c '<APP_DIR>/pre-commands/split_segments_pre.sh "
							+fastq_file.files[0].path
							command += " "+report_file.files[0].path
							command += " "+kraken_file.files[0].path
							var parent = fastq_file.files[0].path.substr(0, fastq_file.files[0].path.lastIndexOf("\/"));
							var base = fastq_file.files[0].path.substr(fastq_file.files[0].path.lastIndexOf("\/")+1);
							base = base.substr(0, base.lastIndexOf("\."));
							command += " "+parent+"/2-split-level/"+base+"/";
							command +=" <APP_DIR>/scripts/split_segments.sh"
							command += "; exec bash'\""							
						}
						else{
							var parent = fastq_file.files[0].path.substr(0, fastq_file.files[0].path.lastIndexOf("\/"));
							var base = fastq_file.files[0].path.substr(fastq_file.files[0].path.lastIndexOf("\/")+1);
							base = base.substr(0, base.lastIndexOf("\."));
							var command="gnome-terminal --command=\"/bin/bash -c 'source <ENV_LOC>/etc/profile.d/conda.sh &&  conda activate jhu_workshop && <APP_DIR>/scripts/split_segments.sh -f "+fastq_file.files[0].path
							+" -r "+report_file.files[0].path
							+" -k "+kraken_file.files[0].path
							+" -o "+parent+"/2-split-level/"
							+base+"/"+"; exec bash'\"";
						}
						execSync(command)
						$("#firstStep").css("background-color", "green")
					}
				})
			}
		}
	}
	else{
		
	}
}
function getFilesMultiple(dir, reg){
	fs.readdirSync(startPath).then((files)=>{
		
	});
}
function checkExistsWithTimeout(filePath, timeout) {
    return new Promise(function (resolve, reject) {

        var timer = setTimeout(function () {
            watcher.close();
            reject(new Error('File did not exists and was not created during the timeout.'));
        }, timeout);

        fs.access(filePath, fs.constants.R_OK, function (err) {
            if (!err) {
                clearTimeout(timer);
                watcher.close();
                resolve();
            }
        });

        var dir = path.dirname(filePath);
        var basename = path.basename(filePath);
        var watcher = fs.watch(dir, function (eventType, filename) {
            if (eventType === 'rename' && filename === basename) {
                clearTimeout(timer);
                watcher.close();
                resolve();
            }
        });
    });
}

function split_level_all(folder){
		
	fs.readdir(folder, (err, files) => {
	  files.forEach(file => {
	    ;
	  });
	});
}


$('#run_all').on("click", function(){
	$("#cancel_group").show()
	run_process_oxford('auto', function(){
		
	})
	

})

function reference_alignment(type,output_dir) {
	$("#thirdStep").css("background-color", "goldenrod")

	if(type=="single"){
		if(!(document.getElementById("split_fastq_file").files[0])){
			const { dialog } = require('electron').remote
			options.message ="No FASTQ File used to align to reference FASTA file"
			dialog.showMessageBox(null, options, (response) => {
			});$("#thirdStep").css("background-color", "firebrick")
		}
		else if(!( document.getElementById("reference_file").files[0])){
			const { dialog } = require('electron').remote
			options.message ="No Reference Fasta File selected"
			dialog.showMessageBox(null, options, (response) => {
			}); $("#thirdStep").css("background-color", "firebrick")
		}
		else{
			var folder = document.getElementById("myInput")
			if(!(folder.files)){
				const { dialog } = require('electron').remote
				options.message ="No run directory specified. Please choose a base directory that contains the fastq_pass folder."
				dialog.showMessageBox(null, options, (response) => {
				});$("#thirdStep").css("background-color", "firebrick")
			} else{
				var output_path = folder.files + "/output/4-reference_alignment/"
				mkdirp(output_path, function(err) { 
					if(err){
						const { dialog } = require('electron').remote
						options.message ="Error in making the directory: "+folder.files + "/output/4-reference_alignment/, Try checking permissions or making it yourself"
						+"Error: "+err
						dialog.showMessageBox(null, options, (response) => {
						});$("#thirdStep").css("background-color", "firebrick")
					}
					else{
						if($('#Workshop_mode').is(':checked')){
							var ref_file = document.getElementById("reference_file").files[0].path
							var split_fastq_file = document.getElementById("split_fastq_file").files[0].path
							var output_path = folder.files + "/output/4-reference_alignment/"
							var fastq_base = split_fastq_file.substr(split_fastq_file.lastIndexOf("\/")+1);
							var output_base = fastq_base.replace(/\.fastq$/, '')
							var output_file = output_path + "/" + output_base
							var command = "gnome-terminal --command=\"/bin/bash -c '<APP_DIR>/pre-commands/minimap2_align.sh "+ref_file+" "+split_fastq_file+" "+output_file+"; exec bash '\""
							execSync(command)
						}
						else{
							var ref_file = document.getElementById("reference_file").files[0].path
							var split_fastq_file = document.getElementById("split_fastq_file").files[0].path
							var output_path = folder.files + "/output/4-reference_alignment/"
							var fastq_base = split_fastq_file.substr(split_fastq_file.lastIndexOf("\/")+1);
							var output_base = fastq_base.replace(/\.fastq$/, '')
							var output_file = output_path + "/" + output_base
							// var command = "gnome-terminal --command=\"/bin/bash -c '<APP_DIR>/pre-commands/minimap2_align.sh "+ref_file+" "+split_fastq_file+" "+output_file+"; exec bash '\""
							var command = "gnome-terminal --command=\"/bin/bash -c 'source <ENV_LOC>/etc/profile.d/conda.sh &&  conda activate jhu_workshop && minimap2 \\\n\t -x map-ont \\\n\t -a \\\n\t -o "+output_file
							+".sam \\\n\t --cs \\\n\t \""+ref_file
							+"\"  \\\n\t \""+split_fastq_file
							+"\" 2> "+output_file+".minimap2.log; exec bash '\""
							execSync(command)
						}
						$("#thirdStep").css("background-color", "green")
					}
				})
			}
		}
	}
	else{
		
	}
}
$('#minimap_align').on("click", function(){
	reference_alignment("single")
})
$('#make_consensus').on("click", function(){
	if(!(document.getElementById("alignment_file").files[0])){
		const { dialog } = require('electron').remote
		options.message ="No alignment file selected (.sam file extension)"
		dialog.showMessageBox(null, options, (response) => {
		});
	}
	else if(!(document.getElementById("reference_file").files[0])){
		const { dialog } = require('electron').remote
		options.message ="No Reference Fasta File selected"
		dialog.showMessageBox(null, options, (response) => {
		});
	}
	else{
		var folder = document.getElementById("myInput")
		if(!(folder.files)){
			const { dialog } = require('electron').remote
			options.message ="No run directory specified. Please choose a base directory that contains the fastq_pass folder."
			dialog.showMessageBox(null, options, (response) => {
			});
		} else{
			make_consensus(folder.files,"single")
		}
	}
})

$('#BLAST_read_top').on("click", function(){
	var folder = document.getElementById("myInput")
	if(!(folder.files)){
		const { dialog } = require('electron').remote
		options.message ="No directory specified for blast formatting. Please choose a base directory from process_oxford"
		dialog.showMessageBox(null, options, (response) => {
		});
	}
	else{
	  var command = "rm -rf "+folder.files+"/output/blast.fasta"+" && awk -F ' '  '{if (FNR==1) { x=FILENAME; gsub(/.*\\//, \"\", x); gsub(/.+/, \">\", $1);print $1x} else if (FNR==2){print}}' "+folder.files +"/output/*.fastq >> "+folder.files+"/output/blast.fasta"
	  exec(command)
	  command = "gnome-terminal --command=\"/bin/bash -c 'source <ENV_LOC>/etc/profile.d/conda.sh &&  conda activate jhu_workshop && <APP_DIR>/scripts/sam2bam.sh "+
	  "&& <APP_DIR>/scripts/bam2fasta.sh "+ 
	  "-r " + folder.files
	  +"-b /output/blast.fasta "
	  +" -o "+folder.files+"/output/blast.out; exec'\"" 
	  exec(command)
	}
})
$('.sbutton_queryNCBI').on("click", function(){
	var format_dir = "/"
	if (this.value == "general"){
		var folder = document.getElementById("myInput")
		format_dir += "output/"
	}
	else{
		var folder = document.getElementById("split_segments_fastq")
	}
	if(!(folder.files)){
		const { dialog } = require('electron').remote
		options.message ="No directory specified for blast formatting. Please choose a base directory (above) from process_oxford output "
		dialog.showMessageBox(null, options, (response) => {
		});
	}
	else{
	  var command = "rm -rf "+folder.files+"/output/blast.fasta"+" && awk -F ' '  '{if (FNR==1) { x=FILENAME; gsub(/.*\\//, \"\", x); gsub(/.+/, \">\", $1);print $1x} else if (FNR==2){print}}' "+folder.files +format_dir+"*.fastq >> "+folder.files+format_dir+"blast.fasta"
	  exec(command)
	  command = "firefox https://blast.ncbi.nlm.nih.gov/Blast.cgi?PAGE_TYPE=BlastSearch"
	  exec(command)
	}
})
document.getElementById("Run_split_segments").addEventListener("click", function(){
	var fastq_file = document.getElementById("fastq_file")
	split_level(fastq_file,"single")
})

$( "[contenteditable=true]" ).each(function(){
	$(this).append("<button class='add'>+</button>")
	$(this).find('.add').click(function(d){
		var row = $(this).parent().find("tr").first().clone()
		$(row).find("td").each(function(){
			this.innerHTML=""
		})
		$("<tr>"+row.html()+"</tr>").insertAfter($(this).parent().find("tr").last())
	})
	$(this).append("<button class='reduce'>-</button>")
	$(this).find(".reduce").click(function(){
		$(this).parent().find("tr").last().remove()
	})
})
$('#Blast_split_segments').on("click", function(){

})

function referenceGeneration(type,outputPath){
	$("#consensusStep").css("background-color", "goldenrod")
	$("#secondStep").css("background-color", "goldenrod")
	if(type=="single"){
		const { dialog } = require('electron').remote
		if (!(document.getElementById("kraken_file2").files[0])){
			options.message ="No kraken report chosen. Please select one from the output directory of the MinION read run (e.g. BC01.kraken.report.full) "
			dialog.showMessageBox(null, options, (response) => {
			}); $("#secondStep").css("background-color", "firebrick")
		}
		else{
			var folder = document.getElementById("myInput")
			if(!(folder.files)){
				options.message ="No run directory specified. Please choose a base directory that contains the fastq_pass folder at the top (overall fastq folder MinKNOW is outputing data to)."
				dialog.showMessageBox(null, options, (response) => {
				}); $("#secondStep").css("background-color", "firebrick")
			} else{
				mkdirp(folder.files + "/output/3-closest-reference", function(err) {
					if(err){
						options.message ="Error in making the directory: "+folder.files + "/output/3-closest-reference, Try checking permissions or making it yourself"
						+"Error: "+err
						dialog.showMessageBox(null, options, (response) => {
						}); $("#secondStep").css("background-color", "firebrick")
					}
					else{
						if($('#Workshop_mode').is(':checked')){
							var kraken_file = document.getElementById("kraken_file2").files[0].path;
							var kraken_DB = dat_loc;
							var parent = kraken_file.substr(0, kraken_file.lastIndexOf("\/"));
							var base = kraken_file.substr(kraken_file.lastIndexOf("\/")+1);
							base = base.substr(0, base.indexOf("\."));
							var reference_fasta = folder.files + "/output/3-closest-reference/"+base+"-closest-reference.fasta";
							var command = "gnome-terminal --command=\"/bin/bash -c '<APP_DIR>/pre-commands/closest_reference.sh "
							+kraken_file +" "+dat_loc+" "
							+reference_fasta+" <APP_DIR>/scripts/get_closest_reference.sh; exec bash'\""
							execSync(command)	
						}
						else{
							var kraken_file = document.getElementById("kraken_file2").files[0].path;
							var kraken_DB = dat_loc;
							var parent = kraken_file.substr(0, kraken_file.lastIndexOf("\/"));
							var base = kraken_file.substr(kraken_file.lastIndexOf("\/")+1);
							base = base.substr(0, base.indexOf("\."));
							var reference_fasta = folder.files + "/output/3-closest-reference/"+base+"-closest-reference.fasta";
							command = "gnome-terminal --command=\"/bin/bash -c 'source <ENV_LOC>/etc/profile.d/conda.sh &&  conda activate jhu_workshop && <APP_DIR>/scripts/get_closest_reference.sh \\\n\t "
							+" -i "+kraken_file
							+" -k "+dat_loc
							+" -o "+reference_fasta+"; exec bash'\""
							execSync(command)
						}
						$("#secondStep").css("background-color", "green")
					}
				})
			}
		}
	}
	else{
		
	}


}

$("#Reference_generation").on("click", function(){
	referenceGeneration("single")

})

//------------------------------------------------
// Run ARTIC pipeline
//------------------------------------------------

$("#artic_gather").on("click", function(){
	artic_gather("single")
})
$("#artic_demultiplex").on("click", function(){
	artic_demultiplex("single")
})
$("#artic_consensus").on("click", function(){
	artic_consensus()
})
function populateNextStrainTable(path){
	try{
		if(fs.existsSync(path)){
			let rawdata = fs.readFile(path, function(err,data){
				let consensusJson = JSON.parse(data)
				Object.keys(consensusJson).forEach(function(entry_id){
					entry = consensusJson[entry_id]
					mutations = ""
					entry.mutations.forEach(function(mut){
						ref = mut.charAt(0)
						mutref = mut.charAt(mut.length -1)
						position = mut.substring(1,mut.length-1)

						mutations+='<p>'+ref+"-"+position+'-'+mutref+'</p>'
					})
					filepath = path.substring(0, path.lastIndexOf("/"));
					const seq_length = entry.sequence.length
					let completeness = entry.unambiguous_nucleotides.toString() +" / "+ seq_length.toString() +
					"<br>("+(100*(entry.unambiguous_nucleotides / seq_length).toFixed(2)).toString() + " %)"

					$('#consensus_table_tbody').empty()
					$('#consensus_table').append("<tr>"+
					"<td class=\"text-center\"><p>"+entry.barcode+"</p></td>"+
					"<td class=\"text-center\"><a  download href="+filepath+"/"+entry.barcode+"-" + entry.reference_header+"><p>"+entry.reference_header+"</p></a></td>"+
					"<td class=\"text-center\"><p>"+entry.reference_acc+"</p></td>"+
					"<td class=\"text-center\"><p>"+entry.reference_length+"</p></td>"+
					"<td class=\"text-center\"><p>"+entry.timestamp+"</p></td>"+
					"<td class=\"text-center\"><p>"+entry.number_of_reads+"</p></td>"+
					"<td class=\"text-center\" ><p>"+completeness+"</p></td>"+
					"<td class=\"text-center\" ><div \"class=\"mutations_div overflow-auto\"<p>"+mutations+"</p></div></td>"+
					"</tr>")
				})
			})
		}
	}
	catch(err){
		console.error("file doesnt exist",err)
	}
}

function artic_consensus_watch(path){
	output_file = path+"/artic_consensus/consensus_report.json"
	populateNextStrainTable(output_file)
	fs.watchFile(output_file, (curr,prev)=>{
		console.log("file changed")
		populateNextStrainTable(output_file)
	})
}
// primer scheme protocol (parse from input directory below)
var protocol = ""

//Input directory with primer scheme
$('#articPrimerScheme').on("click", function(){
	const { dialog } = require('electron').remote
	dialog.showOpenDialog({
		properties: ['openDirectory']
	}).then((dir)=>{
		if (dir.filePaths !== undefined) {
			$("#articPrimerSchemeDot").css("background-color", "green")
			$("#primerSchemeDirectoryText").text(dir.filePaths[0])
			document.getElementById("articPrimerScheme").files = dir.filePaths[0]
		}
	})
})
// run artic gather
function artic_gather(cmd){
	const { dialog } = require('electron').remote
	var folder = document.getElementById("myInput")
	var scheme = document.getElementById("articPrimerScheme")
	if(!(folder.files)){
		options.message ="No run directory specified. Please choose a base directory that contains the fastq_pass folder."
		dialog.showMessageBox(null, options, (response) => {
		});
	}
	else if(!(scheme.files)){
		options.message ="No primer scheme specified. Please choose a primer scheme directory."
		dialog.showMessageBox(null, options, (response) => {
		});
	}
	else{
		const dir = "."
		if (cmd !="auto"){
			$("#articGatherStep").css("background-color", "goldenrod")
			let command ="";

			if($('#Workshop_mode').is(':checked')){
				command = "gnome-terminal --command=\"/bin/bash -c '<APP_DIR>/pre-commands/minion_commands.sh "
				var command_fill = command.concat(" <APP_DIR>/scripts/artic_gather.sh -p "+folder.files+"; exec bash'\"")
				command=command_fill
			}
			else{
				command = "gnome-terminal --command=\"/bin/bash -c 'source <ENV_LOC>/etc/profile.d/conda.sh && conda activate artic-rampart-apl && <APP_DIR>/scripts/artic_gather.sh -p "+folder.files+"; exec bash'\""
			} // var command_fill = command.concat()
			execSync(command)
			$("#articGatherStep").css("background-color", "green")
		}
	}
}

// run artic demultiplex
function artic_demultiplex(cmd){

	const { dialog } = require('electron').remote
	var folder = document.getElementById("myInput")
	var scheme = document.getElementById("articPrimerScheme")
	if(!(folder.files)){
		options.message ="No run directory specified. Please choose a base directory that contains the fastq_pass folder."
		dialog.showMessageBox(null, options, (response) => {
		});
	}
	else if(!(scheme.files)){
		options.message ="No primer scheme specified. Please choose a primer scheme directory. Defaulting to <APP_DIR>/../src/rampart_jhuapl/primer_schemes/nCoV-2019/V1/. Please click demultiplex again if this is okay."
			dialog.showMessageBox(null, options, (response) => {
		});
		document.getElementById("articPrimerScheme").files = "<APP_DIR>/../src/rampart_jhuapl/primer_schemes/nCoV-2019/V1/"
	}
	else{
		const dir = "."
		if (cmd !="auto"){
			$("#articDemultiplexStep").css("background-color", "goldenrod")
			let command ="";

			if($('#Workshop_mode').is(':checked')){
				command = "gnome-terminal --command=\"/bin/bash -c '<APP_DIR>/pre-commands/artic_demultiplex_commands.sh "
				var command_fill = command.concat(" <APP_DIR>/scripts/artic_demultiplex2.sh -p "+folder.files+"; exec bash'\"")
				command=command_fill
			}
			else{
				command = "gnome-terminal --command=\"/bin/bash -c 'source <ENV_LOC>/etc/profile.d/conda.sh && conda activate artic-rampart-apl && <APP_DIR>/scripts/artic_demultiplex2.sh -p "+folder.files+"; exec bash'\""
			} // var command_fill = command.concat()
			execSync(command)
			$("#articDemultiplexStep").css("background-color", "green")
		}
	}
}

// run artic consensus
function artic_consensus(cmd){
	const { dialog } = require('electron').remote
	var folder = document.getElementById("myInput")
	var scheme = document.getElementById("articPrimerScheme")
	if(!(folder.files)){
		options.message ="No run directory specified. Please choose a base directory that contains the fastq_pass folder."
		dialog.showMessageBox(null, options, (response) => {
		});
	}
	else if(!(scheme.files)){
		options.message ="No primer scheme specified. Please choose a primer scheme directory."
		dialog.showMessageBox(null, options, (response) => {
		});
	}
	else{
		const dir = "."
		if (cmd !="auto"){
			$("#articConsensusStep").css("background-color", "goldenrod")
			let command ="";

			if($('#Workshop_mode').is(':checked')){
				command = "gnome-terminal --command=\"/bin/bash -c '<APP_DIR>/pre-commands/artic_consensus_commands.sh "
				var command_fill = command.concat(" <APP_DIR>/scripts/artic_consensus3.sh -p "+folder.files+" -s "+scheme.files+"; exec bash'\"")
				command=command_fill
			}
			else{
				command = "gnome-terminal --command=\"/bin/bash -c 'source <ENV_LOC>/etc/profile.d/conda.sh && conda activate artic-rampart-apl && <APP_DIR>/scripts/artic_consensus3.sh -p "+folder.files+" -s "+scheme.files+"; exec bash'\""
			} // var command_fill = command.concat()
			execSync(command)
			$("#articConsensusStep").css("background-color", "green")
		}
	}
}

