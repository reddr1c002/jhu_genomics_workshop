/* WARNING:

The base version of this script is found in:

	jhu_genomics_workshop/Install_Scripts/main.js

That script is modified for environment variables and copied to:

	jhu_genomics_workshop/app/main.js

All edits should be made in the first script (in Install_Scripts).

 */

// Modules to control application life and create native browser window

const {app, BrowserWindow, Menu, ipcMain, nativeImagge} = require('electron');
const fs = require("fs")
const path = require('path')

const root = fs.readdirSync("<APP_DIR>")
const proc = require("child_process")

const exec = require('child_process').exec;
const execSync = require('child_process').execSync;
const execF = require("child_process").execFile;
const shell = require('electron').shell;
const execute = require("<APP_DIR>/scripts/terminal_execute.js")
const dialog = require("dialog")
const render = require("<APP_DIR>/scripts/beast.js")
require('electron-reload')
require("d3")
const ejs = require('ejs-electron')

 // Enable live reload for Electron too
// require('electron-reload')(__dirname)
// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow
var isMac;
let childWindow
function createWindow () {
  // Create the browser window.
  mainWindow = new BrowserWindow({
    width: 1800,
    height: 1500,
    icon: path.join(__dirname, '<APP_DIR>/img/jhulogo.png'),
    webPreferences: {
      nodeIntegration: true,
      preload: '<APP_DIR>/preload.js'
      // preload: path.join(__dirname, 'preload.js')
    }
  })



  // and load the index.html of the app.
  ejs.data("mytax", {"html": '<APP_DIR>/tabs/mytax-pie/index.html', "local": "tabs/mytax-pie/"})
  mainWindow.loadFile('<APP_DIR>/index.html')

  mainWindow.webContents.once("dom-ready", () =>{

  })


  // Open the DevTools.
  // mainWindow.webContents.openDevTools()

  // Emitted when the window is closed.
  mainWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null
  })



  var menu = Menu.buildFromTemplate([
      {
          label: 'Quick Launch Software',
          submenu: [
              {label:'MinKNOW', 
              click(){
                  show_MinKnow()
                }
              },
              {label:'Sublime Text',
                click(){  
                  show_sublime()
                }
              },
              {label:'ALiView',
                click(){  
                  show_aliview()
                }
              },
              {label:'spreaD3',
                click(){  
                  show_spreaD3()
                }
              },
              {label:'BEAUTI', 
              click(){  
                  // render.show_BEAUTI("<APP_DIR>/pre-commands/")
                  exec("beauti")
                }
              },
              {label:'BEAST', 
              click(){  
                  // render.show_BEAST("<APP_DIR>/pre-commands/")
                  exec("beast")
                }
              },
              {label:'FigTree', 
              click(){  
                  show_figtree()
                }
              },
              {label:'Tempest', 
              click(){  
                  show_tempest()
                }
              },
              {label:'Tracer', 
              click(){  
                  show_tracer()
                }
              },
              {label:'MEGAx', 
                click(){  
                  show_MEGAx()
                }
              }

          ]
      },
       {
    label: 'Edit',
    submenu: [
      { role: 'undo' },
      { role: 'redo' },
      { type: 'separator' },
      { role: 'cut' },
      { role: 'copy' },
      { role: 'paste' },
      ...(isMac ? [
        { role: 'pasteAndMatchStyle' },
        { role: 'delete' },
        { role: 'selectAll' },
        { type: 'separator' },
        {
          label: 'Speech',
          submenu: [
            { role: 'startspeaking' },
            { role: 'stopspeaking' }
          ]
        }
      ] : [
        { role: 'delete' },
        { type: 'separator' },
        { role: 'selectAll' }
      ])
    ]
  },
  // { role: 'viewMenu' }
  {
    label: 'View',
    submenu: [
      { role: 'reload' },
      { role: 'forcereload' },
      { role: 'toggledevtools' },
      { type: 'separator' },
      { role: 'resetzoom' },
      { role: 'zoomin' },
      { role: 'zoomout' },
      { type: 'separator' },
      { role: 'togglefullscreen' }
    ]
  },
  // { role: 'windowMenu' }
  {
    label: 'Window',
    submenu: [
      { role: 'minimize' },
      { role: 'zoom' },
      ...(isMac ? [
        { type: 'separator' },
        { role: 'front' },
        { type: 'separator' },
        { role: 'window' }
      ] : [
        { role: 'close' }
      ])
    ]
  },
  {
    role: 'help',
    submenu: [
      {
        label: 'Learn More',
        click () { require('electron').shell.openExternalSync('https://electronjs.org') }
      }
    ]
  }
  ])
  Menu.setApplicationMenu(menu);

}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') app.quit()
})

app.on('activate', function () {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null){ 

  }
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
function show_Workshop(){
  load.load_Workshop()
}
function show_background(){
   const { BrowserWindow } = require('electron').remote
   let win = new BrowserWindow({ width: 1200, height: 900 })
    win.loadFile('<APP_DIR>/components/background.html')
}

function show_rampart(){
  
}


function show_MinKnow(){
  execF("/opt/ont/minknow-ui/MinKNOW")
}
function show_MEGAx(){
  execF("megax")
}
// function show_BEAST(){
//   exec("gnome-terminal -- bash -c 'beast; exec'")
// }
function show_sublime(){
  execF("sublime_text")
}
function show_aliview(){
  execF("aliview")
}
function show_spreaD3(){
  execF("spreaD3")
}
function show_figtree(){
  execF("figtree")
}
function show_tempest(){
  execF("tempest")
}
function show_tracer(){
  execF("tracer")
}


function scroll2(idmain){
    var elmnt = document.getElementById(idmain);
    var topPos = elmnt.offsetTop;
    pf = elmnt.id
    id = pf.substring(0, pf.length - 6);
    var elmnt = document.getElementById(id); 
    elmnt.scrollIntoView(); 
    const y = document.querySelectorAll("td", function(h){
      h.className="editable"
    })
  }
function configureOpenRenderedLinksInDefaultBrowser() {
    const aAll = document.querySelectorAll("a"); 
    if (aAll && aAll.length) {    
      aAll.forEach(function(a) {      
        a.addEventListener("click", function(event) {        
          if (event.target) {  
            event.preventDefault();   
            let link    
            if (event.target.parentNode.nodeName == "A"){
              link = event.target.parentElement.href
            }
            else{
              link = event.target.href;          
            }
            shell.openExternal(link)    
          }      
        });    
      });  
    }
  }




