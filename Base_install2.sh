#!/bin/bash

# define colors for error messages
red='\033[0;31m'
RED='\033[1;31m'
green='\033[0;32m'
GREEN='\033[1;32m'
yellow='\033[0;33m'
YELLOW='\033[1;33m'
blue='\033[0;34m'
BLUE='\033[1;34m'
purple='\033[0;35m'
PURPLE='\033[1;35m'
cyan='\033[0;36m'
CYAN='\033[1;36m'
NC='\033[0m'

g="$(which python)"
echo $g
if [[ $(echo "$g" | grep "envs/jhu_workshop" | wc -l) -eq 0 ]]; then
	echo -e "\n${RED}ERROR: Please activate the '${YELLOW}jhu_workshop${RED}' conda environment.${NC}\n"
	exit
fi
baseBin="$(dirname $g)"
src_bin="${baseBin}/../src_custom"
envLoc="${baseBin}/../../../"
source $envLoc"/etc/profile.d/conda.sh"
mkdir -p "$src_bin"
#script_location="$(perl -MCwd=abs_path -le 'print abs_path(shift)' $(which $(basename $0)))"
script_location=$(dirname $0)
Database_location="${baseBin}/../databases/"
mkdir -p "$Database_location"
outputPath="$HOME/Desktop/MinION_reports"
if [[ ! -x "$(command -v porechop)" ]] || ([ -L "porechop" ]) && ([ ! -a "porechop" ]); then
	git clone https://github.com/rrwick/Porechop.git $src_bin"/Porechop"
	cd $src_bin"/Porechop"
	python3 setup.py install --prefix=$baseBin"/../"
	cd $script_location
	# if [[ ! -f "$baseBin/porechop" ]]; then
	# 	#contingency plan for installing porechop into the environments (jhu_workshop)
	# 	ln -sf $script_location/src/porechop $baseBin 
	# fi
fi
if [[ ! -x "$(command -v tempest)" ]] || ([ -L "tempest" ]) && ([ ! -a "tempest" ]); then
	#Install tempest locally
	# wget 'http://tree.bio.ed.ac.uk/download.php?id=102&num=3' -P $src_bin"/"
	# tar -xzvf "$src_bin/TempEst_v1.5.3.tgz" --directory "$src_bin/"
	echo "installing tempest via local tgz file"
	tar -xvzf "$script_location/src/TempEst_v1.5.3.tgz" --directory "$src_bin/"
	chmod 755 "$src_bin/TempEst_v1.5.3/bin/tempest"
	ln -sf "$src_bin/TempEst_v1.5.3/bin/tempest" $baseBin"/"
	ln -sf "$src_bin/TempEst_v1.5.3/lib/tempest.jar" "$baseBin/"
fi
if [[ ! -x "$(command -v sublime_text)" ]] || ([ -L "sublime_text" ]) && ([ ! -a "sublime_text" ]); then
	#Install sublime text locally
	echo "installing sublime_text via local tarball"
	tar -xvjf "$script_location/src/sublime_text_3_build_3211_x64.tar.bz2" --directory "$src_bin/"
	chmod 755 "$src_bin/sublime_text_3/sublime_text"
	ln -sf "$src_bin/sublime_text_3/sublime_text" $baseBin"/"
fi
if [[ $(conda env list | grep artic-rampart-apl | wc -l) -eq 0 ]]; then
	if [[ ! -d "$script_location/src/rampart_jhuapl" ]]; then
		git clone https://github.com/Merritt-Brian/rampart.git "$script_location/src/rampart_jhuapl"
	fi
	conda env remove --name artic-rampart-apl # --all -y
	rm -rf "$envLoc/envs/artic-rampart-apl"
	conda env create -f "$script_location/src/rampart_jhuapl/environment.yml"
	#conda activate artic-rampart-apl
	#npm install
	#npm run build
	#cd ../../
fi

# if [[ ! -f "$envLoc/envs/artic-rampart-apl/bin/porechop" ]]; then
# 	#contingency plan for installing porechop into the environments (jhu_workshop)
# 	ln -sf $script_location/src/porechop "$envLoc/envs/artic-rampart-apl/bin/"
# fi

if [[ ! -x "$(command -v aliview)" ]] || ([ -L "aliview" ]) && ([ ! -a "aliview" ]); then
	#aliview
	echo "installing aliview via wget"
	wget http://ormbunkar.se/aliview/downloads/linux/linux-version-1.26/aliview.install.run -P $src_bin'/' 
	chmod +x "$src_bin/aliview.install.run"
	bash "$src_bin/aliview.install.run" --target "$src_bin/aliview" #run program with cmd: aliview
	ln -s "$src_bin/aliview/aliview" "$baseBin"
fi
if [[ ! -x "$(command -v figtree)" ]] || ([ -L "figtree" ]) && ([ ! -a "figtree" ]); then
	#Figtree 
	echo "installing Figtree via wget"
	wget  https://github.com/rambaut/figtree/releases/download/v1.4.4/FigTree_v1.4.4.tgz -P "$src_bin/"
	tar -xzvf "$src_bin/FigTree_v1.4.4.tgz" --directory "$src_bin"
	echo "java -jar \"$src_bin/FigTree_v1.4.4/lib/figtree.jar\"" > ${src_bin}"/figtree.jar"
	ln -sf ${src_bin}"/figtree.jar" ${baseBin}"/figtree"
	chmod 755 ${baseBin}"/figtree"
fi


if [[ ! -x "$(command -v spreaD3)" ]] || ([ -L "spreaD3" ]) && ([ ! -a "spreaD3" ]); then
	#Figtree 
	echo "installing spreaD3 via wget"
	wget  https://rega.kuleuven.be/cev/ecv/software/spread3_files/spread3-v0-9-7-1rc.jar -P "$baseBin/"
	echo "java -jar $baseBin/spread3-v0-9-7-1rc.jar" > ${baseBin}"/spread3.sh"
	ln -sf ${baseBin}"/spread3.sh" ${baseBin}"/spreaD3"
	chmod 755 ${baseBin}"/spreaD3"
fi
#Install the base minikraken database for analysis
# if [[ -z $(find "${Database_location}" -name "minikraken_2017*") ]]; then
# 	wget https://ccb.jhu.edu/software/kraken/dl/minikraken_20171019_4GB.tgz -P $Database_location
# 	tar -xvzf ${Database_location}minikraken_20171019_4GB.tgz  --directory $Database_location
# 	rm ${Database_location}minikraken_20171019_4GB.tgz 
# 	process_krakendb.sh -k ${Database_location}minikraken_20171019_4GB
# fi
if [[ ! -x "$(command -v build_flukraken.sh)" ]] || ([ -L "build_flukraken.sh" ]) && ([ ! -a "build_flukraken.sh" ]); then
	rm -rf "$script_location/app/tabs/mytax"
	git clone https://github.com/tmehoke/mytax.git "$script_location/app/tabs/mytax"
	#Symlink tmehoke flukraken db scripts to $PATH
fi
find "$script_location/app/tabs/mytax" -name "*.sh" -print0 | while read -d $'\0' fn; do ln -sf "$fn" "$baseBin/"; done

if [[ ! -d ${Database_location}"flukraken-2019-11-01" ]]; then
	wget "https://www.dropbox.com/s/w1rx3jp6fp8sf3z/flukraken-2019-06-24.tar.gz?dl=0" -O "$Database_location/flukraken-2019-06-24.tar.gz"
	tar -C "$Database_location" -xzf "$Database_location/flukraken-2019-06-24.tar.gz?dl=0"
	mv "$Database_location/flukraken-2019-06-24" "$Database_location/flukraken-2019-11-01"
#	echo ${Database_location}"flukraken-2019-11-01"
#	echo "building flukraken database in the conda environment"
#	build_flukraken.sh -k ${Database_location}"flukraken-2019-11-01"
#	process_krakendb.sh -k ${Database_location}"flukraken-2019-11-01"
fi


#modify the values in the scripts for apps and copy it over to the app folder from src folder
sed -e "s@<datpath>@${Database_location}@g" \
	-e "s@<APP_DIR>@${script_location}/app/@g" \
	-e "s@<ENV_LOC>@${envLoc}@g" \
	-e "s@<CONDA_DIR>@${baseBin}/../@g" \
	-e "s@<OUTPUT_DIR>@${outputPath}/@g" \
	"$script_location/src/renderer.js" > "$script_location/app/renderer.js"

# add correct path variables to main.js
sed -e "s@<APP_DIR>@${script_location}/app/@g" \
	-e "s@<CONDA_DIR>@${baseBin}/../@g" \
	"$script_location/src/main.js" > "$script_location/app/main.js"

# create launcher and add to favorites
cat << EOF > "$HOME/bin/jhu_workshop"
#!/bin/bash
source "${envLoc}/etc/profile.d/conda.sh"
conda activate jhu_workshop
"$script_location/app/node_modules/electron/dist/electron" "$script_location/app" --no-sandbox
EOF
chmod +x "$HOME/bin/jhu_workshop"
LAUNCHER_dir="$HOME/.local/share/applications"
sed "s@<SCRIPT_LOCATION>@$script_location@g" "$script_location/src/Workshop.desktop" > "$LAUNCHER_dir/jhu_genomics_workshop.desktop"
#gio set "$LAUNCHER_dir/jhu_genomics_workshop.desktop" "metadata::trusted" yes
favorites=$(dconf read /org/gnome/shell/favorite-apps)
# echo "$favorites" | sed "s/\[/\[\'jhu_genomics_workshop.desktop\', /"
dconf write /org/gnome/shell/favorite-apps "$(echo "$favorites" | sed "s/\[/\[\'jhu_genomics_workshop.desktop\', /")"

# add GTK bookmarks
bookmarks="$HOME/.config/gtk-3.0/bookmarks"
if [[ -z $(grep "$script_location/test-data" "$bookmarks") ]]; then
	echo "file:///$script_location/test-data" >> "$bookmarks"
fi
if [[ -z $(grep "$Database_location" "$bookmarks") ]]; then
	echo "file://$Database_location" >> "$bookmarks"
fi
if [[ -z $(grep "$script_location/src/rampart_jhuapl/protocols" "$bookmarks") ]]; then
	echo "file://$script_location/src/rampart_jhuapl/protocols" >> "$bookmarks"
fi
if [[ -z $(grep "$script_location/src/rampart_jhuapl/primer_schemes" "$bookmarks") ]]; then
	echo "file://$script_location/src/rampart_jhuapl/primer_schemes" >> "$bookmarks"
fi

#   # wget:
#   #   https://github.com/rambaut/figtree/releases/download/v1.4.4/FigTree_v1.4.4.tgz -P /tmp/
#   #   http://ormbunkar.se/aliview/downloads/linux/linux-version-1.26/aliview.install.run -P /tmp/ 
args=("$@")
echo ${args[0]}
if [[ ${args[0]} -ne '1' ]]; then
	echo "in"
	echo $script_location
	cd $script_location"/app"
	#install the npm packages 
	npm install electron #to get it to work on WSL you need to uninstall and reinstall to 5.0.4
	npm install d3
	npm install jquery
	npm install mkdirp
	npm install dialog
	npm install electron-reload
	npm install electron-debug
	# npm install tree-kill
	cd ..
fi
