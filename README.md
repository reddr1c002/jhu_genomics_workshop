README

### 1. Install Anaconda/Miniconda 

##### Only do this if you don't have conda installed

https://docs.anaconda.com/anaconda/install/

Examples:

	Ubuntu: 
		wget  https://repo.anaconda.com/archive/Anaconda3-2019.10-Linux-x86_64.sh
		bash Anaconda3-2019.10-Linux-x86_64.sh
		*Follow instructions in the terminal*
	Mac 
		https://docs.anaconda.com/anaconda/install/mac-os/

### 2. Install Standard Dependencies

####*Required*####

To install standard dependencies, `cd to the repository folder (jhu_genomics_workshop)` and run:

	`conda env create -f environment.yml && conda activate jhu_workshop`
	bash Base_install2.sh


Also ensure that you have [gcc](https://linuxize.com/post/how-to-install-gcc-compiler-on-ubuntu-18-04/) and [g++](https://linuxconfig.org/how-to-install-g-the-c-compiler-on-ubuntu-18-04-bionic-beaver-linux) installed on your system. 

Files will be installed in your conda environment (all conda packages, databases, and some additional binary Java files)

or 

in `/usr/bin` 

Some files will also be moved around between the `src` directory and the `app` directory.


### 2.5 MinKNOW and Guppy, Offline version

If you are using Linux or WSL to Install MinKNOW and Guppy...
	`bash MinKNOW_Install.sh`

Otherwise, as a mac user, you need to use the connection-specific one. More information on downloads can be found at: [Downloads at Oxford](https://community.nanoporetech.com/downloads).
You need to be a member of the community in order to install the software




### 3 . Install Megax (OPTIONAL)

Finally, to install megax, go to https://www.megasoftware.net/

Download the Ubuntu Distribution and run 

`sudo dpkg -i path/to/the/deb/package` to Install

If you are using a Mac you just need to dbl-click the file!


### 4. Starting the app

To run via command line, cd to jhu_genomics_workshop/app and ...

`./node_modules/electron/dist/electron .`  or `npm run app` to begin the app!


The installation process will also pin a quickstart icon to your Ubuntu Desktop/toolbar for quicklaunching the application if you're using a debian (Ubuntu) system


##Important

If you are using WSL instead of native ubuntu, please refer to the document: WSL_steps.docx for proper procedures and installation


##Uninstall

To uninstall everything, simply run: `conda remove --name jhu_workshop --all` after you've deactivated your conda environment

and 

`bash uninstall.sh` Note, this command will only work as a Linux/Debian-based user as it only modifies/removes MinKNOW

