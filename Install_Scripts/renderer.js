/* WARNING:

The base version of this script is found in:

	jhu_genomics_workshop/Install_Scripts/renderer.js

That script is modified for environment variables and copied to:

	jhu_genomics_workshop/app/renderer.js

All edits should be made in the first script (in Install_Scripts).

 */

// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.
const {promisify} = require('util')
const homedir = require("os").homedir();
const remote = require('electron').remote
const mkdirp = require('mkdirp');

const readFile = promisify(fs.readFile)

var dat_loc = "<datpath>"
var generate_dat = ""
var orig = dat_loc
const options = {
    type: 'question',
    buttons: ['Cancel'],
    defaultId: 2,
    title: 'Alert there is an error',
    message: '',
  };
///////////////Check if table files are present and override indexhtml elements if so
var old_json = ""
// read_table_files()
// if (fs.existsSync("<APP_DIR>/data/table.txt")) {
// 	fs.readFile("<APP_DIR>/data/table.txt", (err,data)=>{
// 		old_json = JSON.parse(data)
// 		write_table(old_json)
// 	})
// }
// else{
// 	console.log("file doesnt exist for tables, making one")
// 	jso = read_table_files()
// 	old_json = jso
// 	write_table(old_json)
// 	write_file(jso, 0)
// }


// Functions!!////////////////////////////////////////
function show_process(){
  const BrowserWindow = remote.BrowserWindow;
  const win2 = new BrowserWindow({ width: 500, height: 400, x: 800,
        y: 0 })
  win2.loadFile('<APP_DIR>/tabs/fastq/fastq3.html')
  win2.webContents.openDevTools()
	win2.webContents.on("did-finish-load", () =>{
		// console.log(win2.webContents())
		// console.log(remote.getCurrentWindow.getURL())
	})
}
// var x = document.getElementById("database_loc")
// document.getElementById("default_loc").innerText += dat_loc
dat_loc = "<datpath>/flukraken-2019-11-01"

$('#flukraken:checkbox').on("change", function(){
    if ($(this).is(':checked')) {
        dat_loc = "<datpath>/flukraken-2019-11-01"
    }
    else{
    	dat_loc = orig
    }
});
// $('#database_loc').on("change", function(){
// 	dat_loc  = x.files[0].path
// 	dat_loc = this.files
// 	console.log(dat_loc,this)
// 	orig = dat_loc
// 	$("#default_loc").text(dat_loc, "yes")
// })

//Input file for process oxford (contains MinION Reads)
$('#myInput').on("click", function(){
	const { dialog } = require('electron').remote
	dialog.showOpenDialog({
        properties: ['openDirectory']
    }).then((dir)=>{
    	console.log(dir)
		if (dir.filePaths !== undefined) {
			document.getElementById("myInput").files = dir.filePaths[0]
			console.log(dir.filePaths[0]+"/full.log")
			fs.readFile(dir.filePaths[0]+"/full.log",(err,data)=>{
				data = data.toString().split(String.fromCharCode(10));
				var h6 = document.createElement("h6");
				data.forEach(function(d){
					h6.innerHTML += d+"<br>\n"
				})
				$('#logDiv h6').remove()
				$('#logDiv').append(h6);
			})
	    }
	})
})
//Process oxford run on MinION Reads
function run_process_oxford(cmd){
	const { dialog } = require('electron').remote
	var folder = document.getElementById("myInput")
	console.log(folder.files)
	// folder={}
	// folder.files="/jhu_genomics_workshop/test-data"
	if(!(folder.files)){
		options.message ="No run directory specified. Please choose a base directory that contains the fastq_pass folder."
		dialog.showMessageBox(null, options, (response) => {
		});
	}
	else{
		const dir = "."
		console.log(cmd, folder.files, dat_loc)
		if (cmd !="auto"){

			const command = "gnome-terminal --command=\"/bin/bash -c '<APP_DIR>/scripts/process_oxford.sh -p "+folder.files+" -k "+dat_loc+"; exec bash'\""
			// var command_fill = command.concat()
			// console.log(command)
			exec(command)
		}
		else{
			var path = require('path'); 
			fs.stat(folder.files+"/output/1-"+path.basename(dat_loc)+"/done.log",function(err,stat){
				if(err==null){
					// fs.unlinkSync(folder.files+"/output/1-"+path.basename(dat_loc)+"/done.log",function(err){
					// 	if (err) console.log(err);

					// })
				}
			})
			fs.stat(folder.files+"/output/full.log",function(err,stat){
				if(err==null){
					fs.unlinkSync(folder.files+"/output/full.log",function(err){
						if (err) console.log(err);
					})
				}
			})
				
			const command_fill = "bash <APP_DIR>/scripts/process_oxford.sh -p "+folder.files+" -k "+dat_loc +" -l "+folder.files+"/output/1-"+path.basename(dat_loc)+"/process.log"
			+" && touch "+folder.files+"/output/1-"+path.basename(dat_loc)+"/done.log"
			mkdirp.sync(folder.files+"/output/0-porechop")
			mkdirp(folder.files+"/output/1-"+path.basename(dat_loc), function(err) {
				if(err){
					console.log(err)
				}
				else{
					exec(command_fill)
					waitFileComplete(folder.files+"/output/1-"+path.basename(dat_loc)+"/done.log",folder.files,folder.files+"/output/1-"+path.basename(dat_loc)+"/process.log").then((arr)=>{
						console.log("completed")
					}).then(()=>{
						mkdirp(folder.files + "/output/2-split-level", function(err) { 
							if(!err){
								split_level(folder.files,"group")
								waitFileComplete(folder.files+"/output/2-split-level/done.log", folder.files+"/output/2-split-level").then(()=>{
									mkdirp(folder.files + "/output/3-closest-reference", function(err) {
										if(!err){
											referenceGeneration("group",folder.files)
											waitFileComplete(folder.files+"/output/3-closest-reference/done.log", folder.files+"/output/3-closest-reference").then(()=>{
												mkdirp(folder.files + "/output/4-reference_alignment", function(err) {
													if(!err){
														reference_alignment("group",folder.files)
														waitFileComplete(folder.files+"/output/4-reference_alignment/done.log", folder.files+"/output/4-reference_alignment").then(()=>{
															console.log("completed")
															mkdirp(folder.files + "/output/5-consensus-sequences", function(err) {
																if(!err){
																	make_consensus(folder.files,"group")
																	waitFileComplete(folder.files+"/output/5-consensus-sequences/done.log", folder.files+"/output/5-consensus-sequences").then(()=>{
																		console.log("done")
																	})
																}
															})
														})
													}
												})
											})
										}
									})
								})
							}
						})
					})
				}
			})
		}
	}
}
log_string=""
// $("#view_all_log").on("click",function(){
// 	var folder =document.getElementById("myInput")
// 	fs.readFile(folder.files+"/full.log",(e, data) => {
//         if (e) throw e;	
//         $('#logDiv').show()
        
//     });

// })

function appendLogFile(filenameLog,masterLog){
	fs.stat(filenameLog,function(err,stat){
		if(err==null){
			// console.log(masterLog,"iyt====outside copyfile")
			fs.copyFile(filenameLog, masterLog, (err2,data)=> {
			  if (err2) throw err2;
				 fs.readFile(masterLog,"utf8",(err3,data3)=>{
				 	if (err3) throw err;
				 	data3 = data3.toString().split(String.fromCharCode(10));
					var h6 = document.createElement("h6");
					data3.forEach(function(d){
						h6.innerHTML += d+"<br>\n"
					})
					$('#logDiv h6').remove()
					$('#logDiv').append(h6);
					})
			});
			

		}
	})


}

function waitFileComplete(filename,base,log) {
    return new Promise(function (resolve, reject) {
        // var timer = setTimeout(function () {
        //     watcher.close();
        //     reject(new Error('File did not exists and was not created during the timeout.'));
        // }, timeout);
        fs.access(filename, fs.constants.R_OK, function (err) {
            if (!err) {
                // clearTimeout(timer);
                watcher.close();
                resolve();
            }
        });
        if(log){
        	fs.watchFile(log, (curr, prev) => {
        		// console.log(base)
        		appendLogFile(log, base+"/full.log")
			});
        }
        var dir = path.dirname(filename);
        var basename = path.basename(filename);
        var watcher = fs.watch(dir, function (eventType, filename) {
            if (eventType === 'rename' && filename === basename) {
                // clearTimeout(timer);
                // watcher.close();
                resolve();
            }
        });
    });
}

//Find the database location and process it with process_krakendb.sh
$('#database_loc_process').on("click", function(){
	if (orig != dat_loc){
		const command = "gnome-terminal --command=\"/bin/bash -c 'process_krakendb.sh -k "+dat_loc+"; exec bash'\""
		exec(command)
	}
})
$('#database_loc').on("click", function(){
	const { dialog } = require('electron').remote
	 dialog.showOpenDialog({
        properties: ['openDirectory'],
        defaultPath:"<datpath>"
    }).then((dir)=>{
    	if (dir !== undefined) {
            document.getElementById("database_loc").files = dir.filePaths[0]
            dat_loc = dir.filePaths[0]
        }	
    }); 
})
// $('#build_flukraken').on("click", function(){
// 	const { dialog } = require("electron").remote
// 	console.log("clicked")
// 	var path = dialog.showOpenDialog({
// 		properties:['openDirectory']
// 	}).then((dir)=>{
// 		if (dir.filePaths !== undefined) {
// 			console.log(dir.filePaths[0])
// 	        document.getElementById("build_flukraken").files = dir.filePaths
// 	        generate_dat = dir.filePaths[0]
// 	    }
// 	})
// })
$('#build_flukraken_run').on("click", function(){
	generate_dat = "<datpath>"
	if (generate_dat == "" || generate_dat == undefined){
		const { dialog } = require('electron').remote
		options.message ="No location or invalid location selected to generate database in. Please choose a valid folder/location."
			dialog.showMessageBox(null, options, (response) => {
		});
	}
	else{
		console.log("running generation of database")
		const command = "gnome-terminal --command=\"/bin/bash -c 'build_flukraken.sh -k "+generate_dat+"; exec bash'\""
		exec(command)
	}
})

function make_consensus(ref_file, type){
	// console.log("makeconsens: ",type)
	if(type=="single"){
		var folder = document.getElementById("myInput")
		mkdirp(folder.files + "/output/5-consensus-sequences", function(err) { 
			if(err){
				const { dialog } = require('electron').remote
				options.message ="Error in making the directory: "
				+folder.files + "/output/5-consensus-sequences, Try checking permissions or making it yourself"
				"Error: "+err
				dialog.showMessageBox(null, options, (response) => {
				});
			}
			else{
				var ref_file = document.getElementById("reference_file").files[0].path
				var alignment_file = document.getElementById("alignment_file").files[0].path
				var bam_file = alignment_file.replace(/\.sam/, '.bam')
				var alignment_base = alignment_file.substr(alignment_file.lastIndexOf("\/")+1);
				var output_path = folder.files + "/output/5-consensus-sequences"
				var command = "gnome-terminal --command=\"/bin/bash -c '<APP_DIR>/scripts/sam2bam.sh "
				+alignment_file+" && <APP_DIR>/scripts/bam2fasta.sh \\\n\t -r "+ref_file+
				" \\\n\t -b "+bam_file
				+" -o "+output_path+"; exec bash'\""
				// console.log(command)
				exec(command)
			}
		 })
	}
	else{
		folder={}
		folder['files'] = ref_file
		fs.readdir(ref_file+"/output/4-reference_alignment/",(err, files) => {
			files.forEach(file=>{
				if(file.indexOf(".sam") != -1){
					filebase = file.replace(/-.+$/, "")
					alignment_file = file
					var bam_file = alignment_file.replace(/\.sam/, '.bam')
					var command = "<APP_DIR>/scripts/sam2bam.sh "
					+folder.files+"/output/4-reference_alignment/"+alignment_file+" && <APP_DIR>/scripts/bam2fasta.sh -r "+folder.files+"/output/3-closest-reference/"+filebase+"-closest-reference-IAV.fasta"+
					"  -b "+folder.files+"/output/4-reference_alignment/"+bam_file
					+" -o "+folder.files+"/output/5-consensus-sequences/"
					// console.log(command)
					execSync(command)
				}
			})
			execSync("touch "+folder.files+"/output/5-consensus-sequences/done.log")
			console.log("touched 5")
		})
	}
	
}


function split_level(fastq_file,type){
	var report_file = document.getElementById("report_file")
	var kraken_file = document.getElementById("kraken_file")
	const { dialog } = require('electron').remote
	if(type=="single"){
		var folder = document.getElementById("myInput")

		if (!(kraken_file)){
			options.message ="No kraken file chosen. Please select one from the output directory of the MinION read run (e.g. BC01.kraken) "
			dialog.showMessageBox(null, options, (response) => {
			});
		}
		else if (!(fastq_file)){
			options.message ="No fastq file chosen. Please select one from the output directory of the MinION read run (e.g. BC01.fastq) "
			dialog.showMessageBox(null, options, (response) => {
			});
		}
		else if (!(report_file)){
			options.message ="No full string kraken report file chosen. Please select one from the output directory of the MinION read run (e.g. BC01.kraken.report.full) "
			dialog.showMessageBox(null, options, (response) => {
			});
		}
		else{
			
			if(!(folder.files)){
				const { dialog } = require('electron').remote
				options.message ="No run directory specified. Please choose a base directory that contains the fastq_pass folder."
				dialog.showMessageBox(null, options, (response) => {
				});
			} else{
				mkdirp(folder.files + "/output/2-split-level", function(err) { 
					if(err){
						const { dialog } = require('electron').remote
						options.message ="Error in making the directory: "+folder.files + "/output/2-split-level, Try checking permissions or making it yourself"
						+"Error: "+err
						dialog.showMessageBox(null, options, (response) => {
						});
					}
					else{
						var parent = fastq_file.files[0].path.substr(0, fastq_file.files[0].path.lastIndexOf("\/"));
						var base = fastq_file.files[0].path.substr(fastq_file.files[0].path.lastIndexOf("\/")+1);
						base = base.substr(0, base.lastIndexOf("\."));
						var command="gnome-terminal --command=\"/bin/bash -c '<APP_DIR>/scripts/split_segments.sh -f "+fastq_file.files[0].path
						+" -r "+report_file.files[0].path
						+" -k "+kraken_file.files[0].path
						+" -o "+parent+"/2-split-level/"
						+base+"/"+"; exec bash'\"";
						exec(command)
					}
				})
			}
		}
	}
	else{
		folder={}
		folder['files']=fastq_file
		fs.readdir(folder.files+"/output", (err, files) => {
		  files.forEach(file => {
		  	if(file.indexOf(".fastq") != -1){
		  		filebase = file.replace(/\.fastq+$/, "")
			    var command="<APP_DIR>/scripts/split_segments.sh -f "+folder.files+"/output/"+file
				+" -r "+folder.files+"/output/1-"+path.basename(dat_loc)+"/"+filebase+".kraken.report.full"
				+" -k "+folder.files+"/output/1-"+path.basename(dat_loc)+"/"+filebase+".kraken"
				+" -o "+folder.files+"/output/2-split-level/"
				+filebase+"/" 
				execSync(command)
			}
		  });
		  exec("touch "+folder.files+"/output/2-split-level/done.log")
		  console.log("touched at 2")
		})
	}
}
function getFilesMultiple(dir, reg){
	fs.readdirSync(startPath).then((files)=>{
		console.log(files)
	});
}
function checkExistsWithTimeout(filePath, timeout) {
	// console.log(filePath)
    return new Promise(function (resolve, reject) {

        var timer = setTimeout(function () {
            watcher.close();
            reject(new Error('File did not exists and was not created during the timeout.'));
        }, timeout);

        fs.access(filePath, fs.constants.R_OK, function (err) {
            if (!err) {
                clearTimeout(timer);
                watcher.close();
                resolve();
            }
        });

        var dir = path.dirname(filePath);
        var basename = path.basename(filePath);
        var watcher = fs.watch(dir, function (eventType, filename) {
            if (eventType === 'rename' && filename === basename) {
                clearTimeout(timer);
                watcher.close();
                resolve();
            }
        });
    });
}

function split_level_all(folder){
		
	fs.readdir(folder, (err, files) => {
	  files.forEach(file => {
	    console.log(file);
	  });
	});
}


$('#run_all').on("click", function(){
	run_process_oxford('auto', function(){
		console.log("done")
			
	})
	

})

function reference_alignment(type,output_dir) {
	if(type=="single"){
		if(!(document.getElementById("split_fastq_file").files[0])){
			const { dialog } = require('electron').remote
			options.message ="No FASTQ File used to align to reference FASTA file"
			dialog.showMessageBox(null, options, (response) => {
			});
		}
		else if(!( document.getElementById("reference_file").files[0])){
			const { dialog } = require('electron').remote
			options.message ="No Reference Fasta File selected"
			dialog.showMessageBox(null, options, (response) => {
			});
		}
		else{
			var folder = document.getElementById("myInput")
			if(!(folder.files)){
				const { dialog } = require('electron').remote
				options.message ="No run directory specified. Please choose a base directory that contains the fastq_pass folder."
				dialog.showMessageBox(null, options, (response) => {
				});
			} else{
				var output_path = folder.files + "/output/4-reference_alignment/"
				mkdirp(output_path, function(err) { 
					if(err){
						const { dialog } = require('electron').remote
						options.message ="Error in making the directory: "+folder.files + "/output/4-reference_alignment/, Try checking permissions or making it yourself"
						+"Error: "+err
						dialog.showMessageBox(null, options, (response) => {
						});
					}
					else{
						var ref_file = document.getElementById("reference_file").files[0].path
						var split_fastq_file = document.getElementById("split_fastq_file").files[0].path
						var fastq_base = split_fastq_file.substr(split_fastq_file.lastIndexOf("\/")+1);
						var output_base = fastq_base.replace(/\.fastq$/, '')
						var output_file = output_path + "/" + output_base
						// var command = "gnome-terminal --command=\"/bin/bash -c '<APP_DIR>/pre-commands/minimap2_align.sh "+ref_file+" "+split_fastq_file+" "+output_file+"; exec bash '\""
						var command = "gnome-terminal --command=\"/bin/bash -c 'minimap2 \\\n\t -x map-ont \\\n\t -a \\\n\t -o "+output_file
						+".sam \\\n\t --cs \\\n\t \""+ref_file
						+"\"  \\\n\t \""+split_fastq_file
						+"\" 2> "+output_file+".minimap2.log; exec bash '\""
						// console.log(command)
						exec(command)
					}
				})
			}
		}
	}
	else{
		folder= {}
		folder['files']= output_dir
		fs.readdir(output_dir+"/output", (err, files) => {
			  files.forEach(splitFile => {
			  	if (splitFile.indexOf(".fastq") != -1){
			  		filebaseTop = splitFile
			  		filebaseTop=filebaseTop.replace(/\.fastq+$/, "")
					fs.readdir(output_dir+"/output/2-split-level/"+filebaseTop, (err, files2) => {
						files2.forEach(file => {
						  	if(file.indexOf(".fastq") != -1){
						  		filebase = file.replace(/\.fastq+$/, "")
						  		filebaseTop = filebase.replace(/-.+$/,"")
								var output_file = folder.files + "/output/4-reference_alignment/"+ filebase
								var command = "minimap2 -x map-ont -a -o "+output_file
								+".sam --cs \""+folder.files+"/output/3-closest-reference/"+filebaseTop+"-closest-reference-IAV.fasta"
								+"\" \""+folder.files+"/output/2-split-level/"+filebaseTop+"/"+file
								+"\" 2> "+output_file+".minimap2.log"
								// console.log(command)
								exec(command)
							}
						})
					})
				}
		  });
		  exec("touch "+folder.files+"/output/4-reference_alignment/done.log")
		  console.log("touched at 4")
		  return
		  
		})
	}
}
$('#minimap_align').on("click", function(){
	reference_alignment("single")
})
$('#make_consensus').on("click", function(){
	if(!(document.getElementById("alignment_file").files[0])){
		const { dialog } = require('electron').remote
		options.message ="No alignment file selected (.sam file extension)"
		dialog.showMessageBox(null, options, (response) => {
		});
	}
	else if(!(document.getElementById("reference_file").files[0])){
		const { dialog } = require('electron').remote
		options.message ="No Reference Fasta File selected"
		dialog.showMessageBox(null, options, (response) => {
		});
	}
	else{
		var folder = document.getElementById("myInput")
		if(!(folder.files)){
			const { dialog } = require('electron').remote
			options.message ="No run directory specified. Please choose a base directory that contains the fastq_pass folder."
			dialog.showMessageBox(null, options, (response) => {
			});
		} else{
			make_consensus(folder.files,"single")
		}
	}
})

$('#BLAST_read_top').on("click", function(){
	var folder = document.getElementById("myInput")
	if(!(folder.files)){
		const { dialog } = require('electron').remote
		options.message ="No directory specified for blast formatting. Please choose a base directory from process_oxford"
		dialog.showMessageBox(null, options, (response) => {
		});
	}
	else{
	  var command = "rm -rf "+folder.files+"/output/blast.fasta"+" && awk -F ' '  '{if (FNR==1) { x=FILENAME; gsub(/.*\\//, \"\", x); gsub(/.+/, \">\", $1);print $1x} else if (FNR==2){print}}' "+folder.files[0] +"/output/*.fastq >> "+folder.files[0]+"/output/blast.fasta"
	  exec(command)
	  command = "gnome-terminal --command=\"/bin/bash -c '<APP_DIR>/scripts/sam2bam.sh "+
	  "&& <APP_DIR>/scripts/bam2fasta.sh "+ 
	  "-r " + folder.files
	  +"-b /output/blast.fasta "
	  +" -o "+folder.files+"/output/blast.out; exec'\"" 
	  exec(command)
	}
})
$('.sbutton_queryNCBI').on("click", function(){
	var format_dir = "/"
	if (this.value == "general"){
		var folder = document.getElementById("myInput")
		format_dir += "output/"
	}
	else{
		var folder = document.getElementById("split_segments_fastq")
	}
	if(!(folder.files)){
		const { dialog } = require('electron').remote
		options.message ="No directory specified for blast formatting. Please choose a base directory (above) from process_oxford output "
		dialog.showMessageBox(null, options, (response) => {
		});
	}
	else{
	  var command = "rm -rf "+folder.files+"/output/blast.fasta"+" && awk -F ' '  '{if (FNR==1) { x=FILENAME; gsub(/.*\\//, \"\", x); gsub(/.+/, \">\", $1);print $1x} else if (FNR==2){print}}' "+folder.files +format_dir+"*.fastq >> "+folder.files+format_dir+"blast.fasta"
	  exec(command)
	  command = "firefox https://blast.ncbi.nlm.nih.gov/Blast.cgi?PAGE_TYPE=BlastSearch"
	  exec(command)
	}
})
document.getElementById("Run_split_segments").addEventListener("click", function(){
	var fastq_file = document.getElementById("fastq_file")
	split_level(fastq_file,"single")
})

$( "[contenteditable=true]" ).each(function(){
	$(this).append("<button class='add'>+</button>")
	$(this).find('.add').click(function(d){
		var row = $(this).parent().find("tr").first().clone()
		$(row).find("td").each(function(){
			this.innerHTML=""
		})
		$("<tr>"+row.html()+"</tr>").insertAfter($(this).parent().find("tr").last())
	})
	$(this).append("<button class='reduce'>-</button>")
	$(this).find(".reduce").click(function(){
		$(this).parent().find("tr").last().remove()
	})
	// $(this).append("<button class='reset'>Reset</button>")
	// $(this).find(".reset").click(function(){

	// })
})
$('#Blast_split_segments').on("click", function(){

})

function referenceGeneration(type,outputPath){

	if(type=="single"){
		const { dialog } = require('electron').remote
		if (!(document.getElementById("kraken_file2").files[0])){
			options.message ="No kraken report chosen. Please select one from the output directory of the MinION read run (e.g. BC01.kraken.report.full) "
			dialog.showMessageBox(null, options, (response) => {
			});
		}
		else{
			var folder = document.getElementById("myInput")
			if(!(folder.files)){
				options.message ="No run directory specified. Please choose a base directory that contains the fastq_pass folder at the top (overall fastq folder MinKNOW is outputing data to)."
				dialog.showMessageBox(null, options, (response) => {
				});
			} else{
				mkdirp(folder.files + "/output/3-closest-reference", function(err) {
					if(err){
						options.message ="Error in making the directory: "+folder.files + "/output/3-closest-reference, Try checking permissions or making it yourself"
						+"Error: "+err
						dialog.showMessageBox(null, options, (response) => {
						});
					}
					else{
						var kraken_file = document.getElementById("kraken_file2").files[0].path;
						var kraken_DB = dat_loc;
						var parent = kraken_file.substr(0, kraken_file.lastIndexOf("\/"));
						var base = kraken_file.substr(kraken_file.lastIndexOf("\/")+1);
						base = base.substr(0, base.indexOf("\."));
						var reference_fasta = folder.files + "/output/3-closest-reference/"+base+"-closest-reference.fasta";
						// var command = "gnome-terminal --command=\"/bin/bash -c '<APP_DIR>/pre-commands/closest_reference.sh "+kraken_file +" "+dat_loc+" "+reference_fasta+"; exec bash'\""
						command = "gnome-terminal --command=\"/bin/bash -c '<APP_DIR>/scripts/get_closest_reference.sh \\\n\t "
						+" -i "+kraken_file
						+" -k "+dat_loc
						+" -o "+reference_fasta+"; exec bash'\""
						exec(command)
					}
				})
			}
		}
	}
	else{
		folder={}
		folder['files'] = outputPath
		fs.readdir(folder.files+"/output", (err, files) => {
		  files.forEach(file => {
		  	if(file.indexOf(".fastq") != -1){
		  		filebase = file.replace(/\.fastq+$/, "")
				kraken_file = outputPath+"/output/1-"+path.basename(dat_loc)+"/"+filebase+".kraken.report.full"
				var command= "<APP_DIR>/scripts/get_closest_reference.sh"
				+" -i "+kraken_file
				+" -k "+dat_loc
				+" -o "+folder.files + "/output/3-closest-reference/"+filebase+"-closest-reference.fasta";
				// console.log(command)
				exec(command)
			}
		  });
		  execSync("touch "+folder.files+"/output/3-closest-reference/done.log")
		  console.log("touched at 3")
		})
	}


}

$("#Reference_generation").on("click", function(){
	referenceGeneration("single")

})

