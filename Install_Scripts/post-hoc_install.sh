
###Install minknow software from https://github.com/rrwick/MinION-desktop/  github repo
#this will currently only work for linux users
if [ ! -e "/opt/ui/MinKNOW" ]; then	
	#install MinKNOW
	wget -O- https://mirror.oxfordnanoportal.com/apt/ont-repo.pub | sudo apt-key add -
	echo "deb http://mirror.oxfordnanoportal.com/apt xenial-stable non-free" | sudo tee /etc/apt/sources.list.d/nanoporetech.sources.list
	sudo apt-get update
	sudo apt-get -y install minknow-nc
	sudo apt-get -y install minknow-nc

fi



#Enable offline mode for MinKNOW Software
bash linux_offline_minknow.sh
# source $HOME/.bashrc
