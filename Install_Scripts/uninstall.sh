#!/bin/bash


Working_location=$( pwd )
Software_location=$Working_location/Software
KRAKEN_DIR="~/bin/kraken_dir"
BEAST_dir="~/bin/BEAST_dir"
Database_locaton=$Working_location/Software/database/
base='false'
post='false'

while getopts ":pb" opt; do
  case ${opt} in
    p ) 
	echo "removing post-hoc dependencies"
	post='true'
      ;;
    b ) 
	base='true'
	echo "removing all base dependencies"

      ;;
    \? ) echo -e "Usage: cmd 
			[-b]	Will uninstall all base dependencies 
				removing: 
					checkinstall
					pigz
					keras
					noise
					ont-tombo
					mafft
					megax
					pandoc
					jellyfish
					Porechop
					kraken
					expect
					gnome-terminal
					kraken 
					BEAST 
					jellyfish v1 
					expect
					samtools
					bcftools
					tracer
					figtree
					blast
					seqtk

							
					
			[-p]	Will uninstall all post-hoc installs
					gdm3 
					MinKNOW
					tempest
					sublime
					minimap2
					remove kraken database
					"
      ;;
  esac
done


if [[ $base == 'true' ]]; then
	echo "removing base dependencies"
	echo $Software_location
	# exit 1
	# sudo apt -y purge hdf5-tools
	# sudo apt -y purge vim
	# sudo apt -y purge libz-dev

	# pip uninstall -y edlib

	# pip uninstall -y h5py
	# pip uninstall -y pandas
	# sudo apt-get -y purge git
	# sudo apt-get -y purge openjdk-8-jre
	# sudo apt-get -y purge openjdk-8-jdk

	#Remove symlinks/binaries/commands to open/use software
	sudo dpkg --remove mafft #MAFFT
	sudo dpkg --remove megax #MEGAX
	sudo dpkg --remove pandoc #PANDOC
	
	sudo rm -rf $Software_location/Porechop #PORECHOP
	sudo rm -rf ~/bin/porechop-runner.py
	

	sudo rm -rf $KRAKEN_DIR #KRAKEN 
	sudo rm -rf $Software_location/kraken

	sudo rm -rf $HOME/bin/seqtk #Seqtk
	sudo rm -rf $Software_location/seqtk

	sudo rm -rf $HOME/bin/samtools #Samtools
	sudo rm -rf $HOME/bin/samtools.pl \
	$HOME/bin/samtools \
	$HOME/bin/wgsim_eval.pl \
	$HOME/bin/wgsim \
	$HOME/bin/zoom2sam.pl \
	$HOME/bin/seq_cache_populate.pl  \
	$HOME/bin/varfilter.py \
	$HOME/bin/sam2vcf.pl \
	$HOME/bin/soap2sam.pl \
	$HOME/bin/psl2sam.pl \
	$HOME/bin/novo2sam.pl \
	$HOME/bin/md5fa \
	$HOME/bin/md5sum-lite \
	$HOME/bin/maq2sam-short \
	$HOME/bin/psl2sam.pl \
	$HOME/bin/maq2sam-long \
	$HOME/bin/interpolate_sam.pl \
	$HOME/bin/export2sam.pl \
	$HOME/bin/bowtie2sam.pl \
	$HOME/bin/blast2sam.pl \
	$HOME/bin/ace2sam.pl \
	$HOME/bin/plot-bamstats \
	$HOME/bin/ace2sam

	sudo rm -rf $Software_location/bcftools-* #BCFTOOLS
	sudo rm -rf $HOME/bin/vcfutils.pl \
	$HOME/bin/bcftools \
	$HOME/bin/color-chrs.pl \
	$HOME/bin/guess-ploidy.py \
	$HOME/bin/plot-roh.py \
	$HOME/bin/plot-vcfstats \
	$HOME/bin/run-roh.pl



	sudo rm -rf $Software_location/ncbi-blast-* #BLAST
	find $script_location/Software/ncbi-blast-*/bin/ -name "*" -type f -print   | xargs -n 1 -i sh -c "v=\"{}\"; rm  $HOME/bin/\"\${v##*/}\""

	sudo rm -rf $Software_location/aliview* #aliview
	sudo rm -rf /usr/bin/aliview 

	sudo rm -rf $Software_location/jellyfish-1.1.11 #Jellyfish
	sudo rm -rf $HOME/bin/jellyfish


	sudo rm -rf $Software_location/Tracer_* #tracer
	sudo rm -rf $HOME/bin/tracer \
	$HOME/bin/tracer.jar

	sudo rm -rf $Software_location/FigTree_* #Figtree
	sudo rm -rf $HOME/bin/figtree

	sudo rm -rf $Software_location/BEAST* #BEAST
	find $script_location/Software/BEAST*/bin/ -name "*" -type f -print   | xargs -n 1 -i sh -c "v=\"{}\"; rm  $HOME/bin/\"\${v##*/}\""

	sudo rm -rf $Software_location/minimap2 #MINIMAP2
	sudo rm -rf $HOME/bin/minimap2

	find "$script_location/../app/tabs/mytax" -name "*.sh" -type f -print  | xargs -n 1 -i sh -c "v=\"{}\"; rm  $HOME/bin/\"\${v##*/}\"" #mytax scripts
	find "$script_location/../app/scripts" -name "*.sh" -type f -print    | xargs -n 1 -i sh -c "v=\"{}\"; rm  $HOME/bin/\"\${v##*/}\"" #extra scripts

	sudo rm -rf $HOME/Documents/database/flukraken-2019-06-24 #default database deleted

	sudo apt -y purge checkinstall
	sudo apt -y purge pigz




	sudo apt-get -y purge expect
	# sudo apt-get -y purge gawk
	sudo apt-get -y purge lxterminal 
	#Remove all make directories in bin from .bashrc###############
fi
if [[ $post == 'true'  ]]; then
	echo "removing post-hoc dependencies"
	sudo rm -rf /opt/ONT/MinKNOW
	sudo apt-get -y purge ont-bream4-minion
	sudo apt-get -y purge minknow-nc
	sudo apt-get -y purge minknow-core-minion-nc
	sudo apt-get -y purge minion-nc
	sudo apt-get -y purge ont-minknow-frontend
	sudo apt-get -y purge ont-guppy
	sudo rm -rf /opt/ONT/MinKNOW
	#remove minknow makes an authentication error/corruption of gdm3, so purge and reinstall
	# sudo apt-get install gdm3 ubuntu-desktop
	sudo rm -rf $Software_location/database/minikraken*
	sudo rm $Software_location/jellyfish*
	sudo rm $Software_location/mafft*


	# sudo rm -rf $Software_location/TempEst_* #Tempest
	sudo rm -rf $HOME/bin/tempest \
	$HOME/bin/tempest.jar
fi